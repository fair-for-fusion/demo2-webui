# f4f-demo-2
FAIR4Fusion Demonstration 2

This is the main repository for the FAIR4Fusion alternative demonstrator. 

## Deployment

We assume the availability of a Kubernetes cluster and a Gitlab
instance, either the public Gitlab.com service or a private instance
of Gitlab CE. In order to deploy Demonstrator II in a k8s cluster, the
following kubectl apply commands need to be issued. All the
configuration files mentioned below are available in the k8s/ directory:

Core system (Kubernetes operator and NFS storage):

```shell
kubectl apply -f install.yaml
kubectl apply -f deploy.yaml
kubectl apply -f f4f-demo-storage.yaml
```

REST API and Keycloak:

```shell
kubectl apply -f f4f-rest-svc.yaml
kubectl apply -f f4f-rest-dp.yaml
kubectl apply -f keycloak-k8s.yaml
```

WebUI and DB:

```shell
kubectl apply -f f4f-demo-public-svc.yaml
kubectl apply -f f4f-demo-db-ss.yaml
kubectl apply -f f4f-demo-db-svc.yaml
kubectl apply -f f4f-demo-dp.yaml
kubectl apply -f f4f-demo-svc.yaml
```

In order to use Gitlab as the identity service for the REST API - so
that users only need to login to Gitlab to interact with both the
Gitlab image registry and the API - the following configuration is
needed:


## Gitlab Configuration

Login and go to Menu->Your Groups and select your desired group. Note that you will have to be owner for that group. Then:
- Go to Settings->Applications->New Application
- Allow all scopes by ticking all fields
- Under `name` give `f4f-keycloak`
- Under `redirect uri` give your Keycloak URI. This looks like
  `http://accounts.example.org/auth/realms/<realm name>/broker/<identity provider name>/endpoint`
  where `<realm name>` and `<identity provider name>` are as provided
  in the Keycloack configuration.

Save the `Client ID` and `Client Secret` that were provided to you after
completing the registration of the new application. They will be
needed in a later step.


## Keycloak Configuration

To set Gitlab as identity provider for Keycloak, go to Identity
Providers->Add provider, to create a new identity provider At the
Identity Settings tab change the following:
- From the list select `OpenID Connect v1.0`
- In Alias and Display Name give the `<identity provider name>` you used above
- Set `ON` the following options: `Enabled`, `Stored Tokens`,
  `Stored Tokens Readable`, and `Trust Email`
- At option `First Login` choose `first broker login`

Next we will provide the correct URLs that keycloak will hit to
transfer the authentication to gitlab.
- At `Authorization URL` type `https://gitlab.com/oauth/authorize`
- At `Token URL` type `https://gitlab.com/oauth/token`
- At `User Info URL` type `https://gitlab.com/oauth/userinfo`
- At `Cliend ID` and `Client Secret` give the credentials that were
  provided by Gitlab when you created the new application (cf. `Gitlab Configuration` above)
- At `Default Scopes` type all the scopes you allowed when creating
  the application in Gitlab. As a minimum you should allow `read_user`,
  `openid`, `profile`, and `email`
- Click `Save`

Now, we will set a new mapper for a new attribute for the gitlab
provider. The goal here is to transfer the attribute groups that are
provided by gitlab (not to be confused with keycloak groups) after
authentication to the token that keycloak gives back to the
application that requested it. For clarity the new mapper, rules,
and new attribute will be named `f4f-groups` but it can be any
non-conflicting name. At the Mappers tab:
- Click Create.
- At Name give a mapper name, `f4f-groups`
- At Mapper Type choose `Attribute Importer`
- At Claim give `groups`. IMPORTANT: this is the name of the claim
  that Gitlab uses to pass the group information through its token,
  so it needs to be named exactly like this
- At User Attribute Name give the mapper name from above
  (`f4f-groups`). This is the name of the new attribute
- Click `Save`

At this point the identity provider is set. Also a new mapper is set
that maps the attribute groups of Gitlab to a new attribute called
mygroups

Creating a new Client scope for the new attribute f4f-groups: At this
stage we are creating a new scope for the new attribute. It is a
necessary step in order to later give this scope for the clients of
the realm (REST API and Web UI), so they can carry the attribute

- At general tab `Client Scopes` click `Create`
- At `Name` give `f4f-groups`
- Set all options to `ON`
- Click `Save`
- At the new screen click the `Mappers` tab
- Click `Create`
- Set `Name` to `f4f-groups`
- Set `Mapper Type` to `User Attribute`
- Set `User Attribute` to `f4f-groups`. This is the name of the attribute
  in the user info
- Set `Token Claim Name` to `f4f-groups`. This is the claim name that
  will be inserted into the actual token
- Set `Add to ID token`, `Add to access token`, `Add to userinfo` and
  `Multivalued` to `ON`. The `Multivalued` setting must be `ON` because
  the Gitlab groups attribute is a list of all groups that the user
  belongs to in Gitlab. Without setting `Multivalued`, only the first
  group of the list is considered
- Click `Save`

Using this configuration, a user who is logged in Gitlab and is a
member of the Fair4Fusion group can use the WebUI. Furthermore, a user
who is logged in Gitlab can point a browser at the `get_token` function
(in our example, `http://rest-operator.example.org/get_token`)
and will receive a JSON document. The access token is in this document
under the `access_token` key. This access token should be passed in the
header of https requests to the REST API.


## Setting up the database of the Web User Interface

The script `client/client.py` pre-loads the database
with any data it finds under `client/data/`

