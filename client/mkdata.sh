
# Remove header
tail -n +2 data/AUG/summary_time.csv > data1.csv

# shot ids:
cut -d, -f 1 data1 | uniq > shots.list

cat shots.list | \
(while read shot; do
     echo "doing $shot..."
     grep "^${shot}," < data1 | cut -d , -f 6 > b0_mean
     grep "^${shot}," < data1 | cut -d , -f 2 > time
     paste -d' ' b0_mean time > ${shot}.b0_mean
done)

