#!/usr/bin/python

import sys
import json

f = open( sys.argv[1], "r")

# States:
# 1: Collect field name.
# 2: Collect all tab-initial properties.
# 3: Have come across non tab-initial value.
#    Finalize properties and read value.
state = 1

debug = False
data = {}

line = f.readline().rstrip()
line_num = 1
while line:
      if debug: print( "State: " + str(state) + " Doing: " + line)

      if state == 1:
            # Careful: this is a tab, not space
            if line[:1] is '	':
                  print( "ERROR: state: " + str(state) + " line: " + str(line_num) )
                  exit(1)
            else:
                  state = 1
            # TODO: remove the newline
            d = line.split("/")

            cur = data
            while len(d) > 0:
                  field = d.pop(0)
                  if cur.get(field) is None: cur[field] = {}
                  cur = cur[field]
            state = 2

      # Another property.
      elif (state == 2) and (line[:1] is '	'):
            d = line[1:].split(": ")
            if( len(d) ) != 2:
                  print( "ERROR: state: " + str(state) + " line: " + str(line_num) + " len: " + str(lev(fv)))
                  exit(1)
            # Write property
            cur[d[0]] = d[1]
            # If size is 0, then there won't be a value line
            # so next iteration should be in state 1
            if d[0] == "size" and d[1] == "0":
                  state = 1
            else:
                  state = 2

      # No more properties. This line is the value.
      else:
            if cur.get("value") is not None:
                  print( "ERROR: line: " + str(line_num) + " v: " + cur["value"] )
                  exit(1)
            # If the value line has multiple values, write as a list
            d = line.split(" ")
            if len(d) > 1:
                  cur["value"] = d
            else:
                  cur["value"] = line
            state = 1

            # Another check: the value has the declared size
            siz = cur.get("size")
            if (siz is not None) and (siz != len(cur["value"])):
                  print( "ERROR: line: " + str(line_num) + " size: " +  cur["size"] + " len: " + str(len(cur["value"])) )

      line = f.readline().rstrip()
      line_num += 1
# End while

f.close()

if debug: print( json.dumps(data, sort_keys=True, indent=4) )
else: print( json.dumps(data, sort_keys=False) )

