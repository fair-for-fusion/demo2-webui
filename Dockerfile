FROM python:3.7

RUN pip install --upgrade pip
RUN apt update

ARG RELEASE=master
ARG BUILD_DATE

# Install python packages and mysql
RUN apt install -y build-essential git vim libffi-dev default-mysql-client
COPY  ./requirements.txt /f4f-demo-2/
RUN pip install -r /f4f-demo-2/requirements.txt && \
    pip install gunicorn && \
    rm -rf /f4f-demo-2/.git && rm -rf f4f-demo-2/docs && rm -rf /f4f-demo-2/container && rm -rf /f4f-demo-2/k8s
RUN pip install datapunt_keycloak_oidc
RUN pip install pyjwt

COPY . /f4f-demo-2

WORKDIR /f4f-demo-2/f4f_demo_site
COPY ./container/docker_entrypoint.sh .
COPY ./container/celery.service /etc/systemd/system/celery.service
COPY ./container/celery /etc/conf.d/celery
COPY ./container/celeryd /etc/init.d/celery

EXPOSE 8000
CMD bash docker_entrypoint.sh
