apiVersion: v1
kind: Namespace
metadata:
  labels:
    control-plane: controller-manager
  name: f4f-operator-system
---
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.2.5
  creationTimestamp: null
  name: f4fs.webapp.my.domain
spec:
  group: webapp.my.domain
  names:
    kind: F4F
    listKind: F4FList
    plural: f4fs
    singular: f4f
  scope: Namespaced
  subresources:
    status: {}
  validation:
    openAPIV3Schema:
      description: F4F is the Schema for the f4fs API
      properties:
        apiVersion:
          description: 'APIVersion defines the versioned schema of this representation of an object. Servers should convert recognized schemas to the latest internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
          type: string
        kind:
          description: 'Kind is a string value representing the REST resource this object represents. Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
          type: string
        metadata:
          type: object
        spec:
          properties:
            Phase:
              description: Mapping phase is set to true if we currently are in mapping phase.When we proceed to reduce phase, it becomes false
              format: int32
              type: integer
            comparisonType:
              type: string
            experimentId:
              type: string
            work:
              items:
                properties:
                  image:
                    type: string
                  name:
                    type: string
                  workLabel:
                    type: string
                required:
                - image
                - name
                type: object
              type: array
          required:
          - Phase
          - work
          type: object
        status:
          description: F4FStatus defines the observed state of F4F
          properties:
            count:
              description: 'INSERT ADDITIONAL STATUS FIELD - define observed state of cluster Important: Run "make" to regenerate code after modifying this file Count is the number of nodes the image is deployed to'
              format: int32
              type: integer
          required:
          - count
          type: object
      type: object
  version: v1
  versions:
  - name: v1
    served: true
    storage: true
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: f4f-operator-leader-election-role
  namespace: f4f-operator-system
rules:
- apiGroups:
  - ""
  resources:
  - configmaps
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch
  - delete
- apiGroups:
  - ""
  resources:
  - configmaps/status
  verbs:
  - get
  - update
  - patch
- apiGroups:
  - ""
  resources:
  - events
  verbs:
  - create
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  creationTimestamp: null
  name: f4f-operator-manager-role
rules:
- apiGroups:
  - ""
  resources:
  - jobs
  - nodes
  - pods
  verbs:
  - '*'
- apiGroups:
  - apps
  resources:
  - daemonsets
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - batch
  resources:
  - jobs
  - nodes
  - pods
  verbs:
  - '*'
- apiGroups:
  - webapp.my.domain
  resources:
  - f4fs
  verbs:
  - '*'
- apiGroups:
  - webapp.my.domain
  resources:
  - f4fs/status
  verbs:
  - '*'
- apiGroups:
  - webappv1
  resources:
  - jobs
  - nodes
  - pods
  verbs:
  - '*'
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: f4f-operator-proxy-role
rules:
- apiGroups:
  - authentication.k8s.io
  resources:
  - tokenreviews
  verbs:
  - create
- apiGroups:
  - authorization.k8s.io
  resources:
  - subjectaccessreviews
  verbs:
  - create
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: f4f-operator-metrics-reader
rules:
- nonResourceURLs:
  - /metrics
  verbs:
  - get
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: f4f-operator-leader-election-rolebinding
  namespace: f4f-operator-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: f4f-operator-leader-election-role
subjects:
- kind: ServiceAccount
  name: default
  namespace: f4f-operator-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: f4f-operator-manager-rolebinding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: f4f-operator-manager-role
subjects:
- kind: ServiceAccount
  name: default
  namespace: f4f-operator-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: f4f-operator-proxy-rolebinding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: f4f-operator-proxy-role
subjects:
- kind: ServiceAccount
  name: default
  namespace: f4f-operator-system
---
apiVersion: v1
kind: Service
metadata:
  labels:
    control-plane: controller-manager
  name: f4f-operator-controller-manager-metrics-service
  namespace: f4f-operator-system
spec:
  ports:
  - name: https
    port: 8443
    targetPort: https
  selector:
    control-plane: controller-manager
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    control-plane: controller-manager
  name: f4f-operator-controller-manager
  namespace: f4f-operator-system
spec:
  replicas: 1
  selector:
    matchLabels:
      control-plane: controller-manager
  template:
    metadata:
      labels:
        control-plane: controller-manager
    spec:
      containers:
      - args:
        - --secure-listen-address=0.0.0.0:8443
        - --upstream=http://127.0.0.1:8080/
        - --logtostderr=true
        - --v=10
        image: gcr.io/kubebuilder/kube-rbac-proxy:v0.5.0
        name: kube-rbac-proxy
        ports:
        - containerPort: 8443
          name: https
      - args:
        - --metrics-addr=127.0.0.1:8080
        - --enable-leader-election
        command:
        - /manager
        image: registry.gitlab.com/fair4fusion/deployment/f4f-operator
        name: manager
        resources:
          limits:
            cpu: 100m
            memory: 30Mi
          requests:
            cpu: 100m
            memory: 20Mi
      terminationGracePeriodSeconds: 10
