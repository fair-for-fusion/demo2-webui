port=8000
exec_path=./

# Registry db initializations
until python $exec_path/manage.py makemigrations ;do #> /dev/null 2>&1 ;do
      echo "Waiting mysql docker to setup........"
      sleep 1
done

echo "Initializing....."
python $exec_path/manage.py makemigrations
python $exec_path/manage.py migrate
python $exec_path/manage.py migrate --run-syncdb
python $exec_path/manage.py collectstatic

username="root"
password="root"
dbname="f4f"

email=$username"@example.com"

echo "Creating super user....."

echo "from django.contrib.auth.models import User; User.objects.create_superuser('$username', '$email', '$password')" | python $exec_path/manage.py shell

echo "Add FULLTEXT search"
mysql -u "${username}" -p"${password}" -h "${F4F_DEMO_DB_SERVICE_HOST}" -P "${F4F_DEMO_DB_SERVICE_PORT}" -D "${dbname}" -e "ALTER TABLE demo_pulse ADD FULLTEXT ftidx (cannots);"
mysql -u "${username}" -p"${password}" -h "${F4F_DEMO_DB_SERVICE_HOST}" -P "${F4F_DEMO_DB_SERVICE_PORT}" -D "${dbname}" -e "ALTER TABLE demo_pid ADD FULLTEXT ftidx_pid (search_description);"

echo "Starting celery"
#celery -A f4f_demo_site worker --broker "amqp://f4f:f4f@$RABBITMQ_SERVICE_HOST:$RABBITMQ_SERVICE_PORT//" -l debug &
celery multi start worker1 \
    -A f4f_demo_site \
    --pidfile="$HOME/run/celery/%n.pid" \
    --logfile="$HOME/log/celery/%n%I.log"

echo "Starting web server...."

exec gunicorn  --preload -w 9 -b 0.0.0.0:$port f4f_demo_site.wsgi \
		--log-level debug \
                --backlog 0 \
                --timeout 1200