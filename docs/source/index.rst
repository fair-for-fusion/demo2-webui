.. Fair4Fusion Demonstrator documentation master file, created by
   sphinx-quickstart on Sat Jul  4 09:42:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fair4Fusion Demonstrator's documentation!
====================================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   installation
   config
   models
   rest
   ui



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
