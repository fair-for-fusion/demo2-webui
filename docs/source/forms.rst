Search Form View for F4F Demonstrator
=====================================

.. automodule:: demo_ui.forms
   :members:
   :private-members:
   :special-members:
   :inherited-members:

   .. automethod:: __init__