################################
Installation of F4F Demonstrator
################################

The F4F Demonstrator contains three different applications
    1. The data layer with the models that represent the DB tables
    2. The RESTful Web Service which exposes functionality on the DB data via HTTP requests
    3. The User Interface layer

You can install the demonstrator with three different ways. The first way is to use the yaml files in the k8s folder
of the repository. Before the installation, kubernetes should be available in the infrastructure. Also, since this
is a private repository you need the GitLab deploy token associated with this repository and the docker image stored
in the repository's registry.

Therefore, before creating the deployments and services using the yaml files and after having kubernetes setup,
you should execute the following:

```bash
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=demo --docker-password=uSJG83ELDFsRaS7q132b
```
Now, you can just use the deploy.sh script under the k8s folder to deploy the demonstrator.

The second way is to use docker compose to deploy one container with mysql and one container with the demonstrator. Since
the image is private, you should again provide the deploy token

Finally, you can just deploy the application locally. Firstly, you need is to create a MySQL schema named f4f and execute
the sql script in the f4f_demo_site/db folder, i.e. the add_fulltext_index.sql. Also, rabbitmq and celery should be
present in the infrastructure since they are used for data ingestion in the DB.

Regardless of the deployment method you have selected, the next step is to configure the application. If you have selected
the local installation, go to the f4f_demo_site/f4f_demo_site directory and create a new file named properties.json.
Copy the content of the example_properties.json inside the new file. For the other cases, modify the example_properties
directly and build a new image that should be pushed in the GitLab registry.
The properties that should be changed, if necessary, are the following:

    1. plot section: this is an application specific property and contains configuration regarding the plots that appear in the pulses detailed view. You can specify which time series will be plotted, as well as the size of the plots.
    2. DATABASES section: update the default database in the configuration file by changing the host, username and password.
        i. If you want to use a test database change the use_database field from default to test and update the configuration of the test database
    3. ALLOWED_HOSTS: this field contains the IPs of allowed hosts. Use '*' to allow all hosts
    4. DEBUG: boolean parameter to specify if the application runs in production or test mode

Finally, for the manual deployment (i.e. if you do not use the docker image), you should also run the following:

```
cd f4f_demo_site &&
python manage.py makemigrations &&
python manage.py migrate &&
python manage.py migrate --run-syncdb &&
python manage.py createsuperuser &&
python manage.py collectstatic
```

Now, the setup steps are performed and you can run the application. However, the database will be empty. If you want
to populate the MySQL database with pulses data, go to the client folder. Inside, you will find the data directory.
Create a folder named after your machine (e.g. West, AUG, etc) and inside include two CSV files,
i.e. summary.csv and summary_time.csv. In order to populate the DB, you should be authenticated in the system.
Copy the example_credentials.yaml in a new file named credentials.yaml and add your username and password as well as
the base URL (IP and port) to the F4F Demonstrator. Finally, use the client.py script to populate the DB. So far,
the demonstrator can read data in the format provided in the data folder, i.e. from the AUG machine. In the future,
the API will be updated to read pulse data from other machines.

The Demonstrator is not open to anyone for the moment. The users should have an account in order to login. For the
moment, the user accounts are created by the admin and the registration view is disabled. The Demonstrator uses the
dare-platform. We use Kubernetes and Docker containers to deploy all the services that follow the Microservices
Architecture. In the DARE platform, there is a component named dare-login which handles all the login, authentication,
refresh session etc requests. It uses Keycloak as backend. Therefore, for the successful validation and authentication
of the users as well as for handling the user accounts, the DARE platform should be present in the infrastructure. The
administrators can create new user accounts from the Keycloak Dashboard.

## Data ingestion

To populate data in the DB use the client script in the client directory. Before executing it, you will need to configure the RabbitMQ and run a celery worker:

```shell
rabbitmqctl add_user f4f f4f
rabbitmqctl set_user_tags f4f administrator
rabbitmqctl set_permissions -p / f4f ".*" ".*" ".*"
```

`celery -A f4f_demo_site worker --broker="amqp://f4f:f4f@localhost:5672//" --loglevel=debug`


pbkdf2_sha256$216000$DW6mIlYVZ1ni$E4fmuThGKjoV8JB5ReWF5kW52PAeLOT57B626J0EHCc=