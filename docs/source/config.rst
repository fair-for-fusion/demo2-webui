Demonstrator configuration
===============================

.. toctree::
    :caption: Configuration & Installation

.. automodule:: f4f_demo_site.settings
   :members: