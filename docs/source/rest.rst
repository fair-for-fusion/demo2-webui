RESTful API for F4F Demonstrator
================================

.. toctree::
        butils
        tasks

.. automodule:: demoapi.views
   :members:
   :undoc-members:
