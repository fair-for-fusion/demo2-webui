Model Serializers for F4F Demonstrator
======================================

.. automodule:: demoapi.serializers
   :members:
   :undoc-members:
