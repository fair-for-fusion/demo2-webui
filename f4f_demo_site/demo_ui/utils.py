import json
from collections import OrderedDict
from datetime import datetime
from time import time
from django.middleware.csrf import logger

import numpy as np
import requests
from bokeh.embed import components
from bokeh.plotting import figure
from bokeh.palettes import Spectral11

from f4f_demo_site.settings import __version__, API_BASE_URL, properties, EXEC_API_URL


def get_search_data(search_txt, start_date, end_date, machine, owner, token, page=None):
    """
    Function used to get paged data based on a user's search. Makes use of the RESTful API to retrieve
    the filtered results

    Args
        | search_txt (str): free text retrieved from the "Search for" field of the form
        | start_date (str): the start date that will be used to filter the results
        | end_date (str): the upper bound of creation date to filter the result
        | machine (str): the experiment / machine name
        | owner (str): the username of the pulse owner
        | page (int): the number of the page with the results

    Returns
        list: paged result based on the user's search
    """
    data = {
        "access_token": token,
        "search_for": search_txt,
        "start_date": start_date,
        "end_date": end_date,
        "machine": machine,
        "owner": owner,
        "page": page
    }
    return requests.get(API_BASE_URL + "/api/pulses/", params=data).json()


def get_machine_data(token, machine=None, page=None):
    """
    Function that uses the paged view in the backend to list the available machines. If search filters are provided
    they are passed as parameters in the request

    Args
        | machine (str): the value from the drop down menu in the machine search form
        | short_name (str): the value of the search box at the top right of the page
        | page (int): the number of the page with the results

    Returns
        | list: the list of the respective machines / experiments
    """
    data = {"access_token": token}
    if machine:
        data["machine"] = machine
    if page:
        data["page"] = page
    return requests.get(API_BASE_URL + "/api/experiments-paged", params=data).json()


def get_pid_data(token, pid_number=None, search_txt=None, issuer=None, page=None):
    """
    Function to get a list of the available PIDs. If there are filter parameters they are passed in the request as
    parameters

    Args
        | pid_number (str): the number of the PID (value from the top right search box)
        | search_txt (str): free text to search a pid's description or page url
        | issuer (str): the value of the drop down menu with the issuers
        | page (int): the number of the page with the results

    Returns
        | list: the list of the respective PIDs
    """
    data = {"access_token": token}
    if pid_number:
        data["pid_number"] = pid_number
    elif search_txt or issuer:
        data["search_txt"] = search_txt
        data["issuer"] = issuer

    if page:
        data["page"] = page
    return requests.get(API_BASE_URL + "/api/pids-paged", params=data).json()


def get_user_data(token, username=None, first_name=None, last_name=None, email=None, page=None):
    """
    Function to list application users. The filtering parameters are passed as parameter

    Args
        | username (str): the selected username (either from the top right search box or from the dropdown)
        | first_name (str): the selected first name from the dropdown menu
        | last_name (str): the selected last name from the dropdown menu
        | email (str): the selected email from the menu
        | page (str): the number of the page with the results

    Returns
        | list: the respective users list
    """
    data = {"access_token": token, "username": username, "first_name": first_name, "last_name": last_name,
            "email": email, "page": page}
    return requests.get(API_BASE_URL + "/api/users-paged", params=data).json()


def get_pulses_in_pid(pid_id, token, page=None):
    """
    Function that returns all the pulses under a PID

    Args
        | pid_id (str): the ID of the PID
        | page (int): the number of the page to use in order to retrieve paged data

    Returns
        list: paged result based on the PID
    """
    data = {
        "access_token": token,
        "pid_id": pid_id,
        "page": page
    }
    return requests.get(API_BASE_URL + "/api/pulses/", params=data).json()


def get_pulses_by_shot(shot_number, token, page=None):
    """
    Function that returns all the pulses based on the shot number

    Args
        | shot (int): the shot number
        | page (int): the number of the page to use in order to retrieve paged data

    Returns
        list: paged result based on the PID
    """
    data = {
        "access_token": token,
        "shot_number": shot_number,
        "page": page
    }
    return requests.get(API_BASE_URL + "/api/pulses/", params=data).json()


def collect_machine_names(token):
    """
    Function that retrieves all the machine names available in the DB for the drop down list initialization.
    Makes use of the RESTful view for the Experiment Django model

    Returns
        | list: a list of all the available machine names
    """
    machines = requests.get(API_BASE_URL + "/api/experiments/", params={"access_token": token}).json()
    machines = machines["results"]
    machine_names = []
    for machine in machines:
        machine_names.append(machine["short_name"])
    return machine_names


def collect_pid_issuers(token):
    """
    Function to get distinct the issuers name from the DB (used in the dropdown filter form for the PIDs)

    Returns
        | list: a list of the distinct issuers from the DB
    """
    issuers = requests.get(API_BASE_URL + "/api/pids/issuers-distinct", params={"access_token": token})
    if issuers.status_code == 200:
        issuers = json.loads(json.loads(issuers.text))
        return issuers["issuers"]


def collect_usernames(token):
    """
    Function that retrieves all the owner usernames available in the DB for the drop down list initialization.
    Makes use of the RESTful view for the User Django model

    Returns
        | list: a list with all the available usernames
    """
    users = requests.get(API_BASE_URL + "/api/users/", params={"access_token": token}).json()
    users = users["results"]
    usernames = []
    for user in users:
        usernames.append(user["username"])
    return usernames


def collect_user_data(token):
    """
    Function to get distinct the available usernames, last names, first names and emails from the DB. The data are used
    for the list view of users (in the search form - dropdown menus)

    Returns
        | dict: dictionary with the usernames, the first names, last names and emails
    """
    user_data = {"usernames": [], "first_names": [], "last_names": [], "emails": []}
    user_data_resp = requests.get(API_BASE_URL + "/api/users/data-distinct", params={"access_token": token})
    if user_data_resp.status_code == 200:
        user_data_resp = json.loads(json.loads(user_data_resp.text))
        if user_data_resp["usernames"]:
            user_data["usernames"] = user_data_resp["usernames"]
        if user_data_resp["first_names"]:
            user_data["first_names"] = user_data_resp["first_names"]
        if user_data_resp["last_names"]:
            user_data["last_names"] = user_data_resp["last_names"]
        if user_data_resp["emails"]:
            user_data["emails"] = user_data_resp["emails"]
    return user_data


def transform_annotations(annotations, pulse_format="str", limit_str=10):
    """
    Function that accesses the cannots field of a pulse object and returns the annations either as list of strings
    or as string concatenated with new lines.

    Args
        | concat_annotations (str): the cannots field of a pulse
        | pulse_format (str): indicates the type of the result, i.e. string, detail or list. The string format is used
        in the pulses list view in the column annotations, the list format is used in the expandable rows of the
        same table while the detail format is used in the pulse detail vue (in the annotations tab)

    Returns
        | list or string: the annotations associated with a pulse in a string or list format
    """
    if pulse_format == "str":
        annotation_txt = ""
        for annotation in annotations:
            words = annotation["text"].split(" ")
            if len(words) > limit_str:
                if annotation_txt:
                    txt_words = annotation_txt.split(" ")
                    words = txt_words + words
                    if len(words) > limit_str:
                        words = words[:limit_str]
                else:
                    words = words[:limit_str]
            else:
                if annotation_txt:
                    txt_words = annotation_txt.split(" ")
                    words = txt_words + words
                    if len(words) > limit_str:
                        words = words[:limit_str]

            annotation_txt = " ".join(words)
        return annotation_txt
    elif pulse_format == "list":
        annotations_txt = []
        annotations = sorted(annotations, key=lambda k: k["time"], reverse=True)
        for annotation in annotations:
            annotation_date = datetime.strptime(annotation["time"].split(".")[0], '%Y-%m-%dT%H:%M:%S').date().strftime(
                "%d-%m-%Y")
            annotation_time = datetime.strptime(annotation["time"].split(".")[0], '%Y-%m-%dT%H:%M:%S').time().strftime(
                "%H:%M:%S")
            annotation_txt = "@{} ({} {}): {} ".format(annotation["user"]["username"], annotation_date, annotation_time,
                                                       annotation["text"])
            annotations_txt.append(annotation_txt)
        return annotations_txt
    elif pulse_format == "detail":
        grouped_annotations = {}
        for annotation in annotations:
            annotation_date = datetime.strptime(annotation["time"].split(".")[0], '%Y-%m-%dT%H:%M:%S').date().strftime(
                "%d-%m-%Y")
            annotation_time = datetime.strptime(annotation["time"].split(".")[0], '%Y-%m-%dT%H:%M:%S').time().strftime(
                "%H:%M:%S")
            if annotation_date not in grouped_annotations.keys():
                grouped_annotations[annotation_date] = []
            annotation_dict = {"user": annotation["user"]["username"], "time": annotation_time,
                               "text": annotation["text"]}
            grouped_annotations[annotation_date].append(annotation_dict)

        for date, annotations in grouped_annotations.items():
            annotations = sorted(annotations, key=lambda k: k["time"])
            grouped_annotations[date] = annotations
        grouped_annotations = OrderedDict(sorted(grouped_annotations.items()))
        previous_color = "white"
        for date, annotations in grouped_annotations.items():
            for annotation in annotations:
                annotation["color"] = previous_color
                previous_color = "black" if previous_color == "white" else "white"
        return grouped_annotations


def version_info():
    """
    Returns the demonstrator's version
    """
    return __version__


def calc_analytics(pulse, fields_to_plot, token):
    data = collect_data(pulse, fields_to_plot)
    analytics_properties = properties["analytics"]
    run_dir = ""
    if analytics_properties["workflow"] == "d4p":
        dare_properties = analytics_properties["d4p"]
        run_dir = submit_d4p(impl_id=dare_properties["impl_id"], pckg=dare_properties["pckg"],
                             workspace_id=dare_properties["workspace_id"], pe_name=dare_properties["pe_name"],
                             n_nodes=1, token=token, hostname=EXEC_API_URL, reqs=dare_properties["reqs"],
                             inputdata=data, no_processes=1, iterations=1, target="simple")
    elif analytics_properties["workflow"] == "cwl":
        dare_properties = analytics_properties["cwl"]
        run_dir = submit_cwl(hostname=EXEC_API_URL, token=token, workflow_name=dare_properties["workflow_name"],
                             workflow_version=dare_properties["workflow_version"], input_data=data, shot=pulse["shot"])
    return run_dir


def collect_data(pulse, fields_to_plot):
    data = {}
    for field_to_plot in fields_to_plot:
        if field_to_plot == "B0 value":
            if pulse["s_globalQuantities_b0_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_b0_value"]["values"]
        elif field_to_plot == "Power nbi":
            if pulse["s_heatingCurrentDrive_powerNbi_value"]["values"]:
                data[field_to_plot] = pulse["s_heatingCurrentDrive_powerNbi_value"]["values"]
        elif field_to_plot == "Neutron Fluxes":
            if pulse["s_fusion_neutronFluxes_total_value"]["values"]:
                data[field_to_plot] = pulse["s_fusion_neutronFluxes_total_value"]["values"]
        elif field_to_plot == "Beta Pol value":
            if pulse["s_globalQuantities_betapol_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_betapol_value"]["values"]
        elif field_to_plot == "Beta Tor value":
            if pulse["s_globalQuantities_betaTor_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_betaTor_value"]["values"]
        elif field_to_plot == "Current Ohm value":
            if pulse["s_globalQuantities_currentOhm_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_currentOhm_value"]["values"]
        elif field_to_plot == "Energy BField Pol value":
            if pulse["s_globalQuantities_energyBFieldPol_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_energyBFieldPol_value"]["values"]
        elif field_to_plot == "Energy Diamagnetic value":
            if pulse["s_globalQuantities_energyDiamagnetic_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_energyDiamagnetic_value"]["values"]
        elif field_to_plot == "Energy Total value":
            if pulse["s_globalQuantities_energyTotal_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_energyTotal_value"]["values"]
        elif field_to_plot == "S Time Width":
            if pulse["s_timeWidth"]["values"]:
                data[field_to_plot] = pulse["s_timeWidth"]["values"]
        elif field_to_plot == "S Time":
            if pulse["s_time"]["values"]:
                data[field_to_plot] = pulse["s_time"]["values"]
        elif field_to_plot == "Ip value":
            if pulse["s_globalQuantities_ip_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_ip_value"]["values"]
        elif field_to_plot == "Li value":
            if pulse["s_globalQuantities_li_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_li_value"]["values"]
        elif field_to_plot == "Tau Energy value":
            if pulse["s_globalQuantities_tauEnergy_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_tauEnergy_value"]["values"]
        elif field_to_plot == "Volume value":
            if pulse["s_globalQuantities_volume_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_volume_value"]["values"]
        elif field_to_plot == "VLoop_value":
            if pulse["s_globalQuantities_vLoop_value"]["values"]:
                data[field_to_plot] = pulse["s_globalQuantities_vLoop_value"]["values"]
        elif field_to_plot == "Volume Average NE value":
            if pulse["s_volumeAverage_nE_value"]["values"]:
                data[field_to_plot] = pulse["s_volumeAverage_nE_value"]["values"]
    return data


def visualize_parameters(pulse, fields_to_plot, width, height):
    """
    Based on the configuration parameters, read on startup, the demonstrator decides which pulse fields of JSON type
    will plot in the DetailView

    Args
        | pulse (Pulse): the pulse object selected to be shown in the detailed view
        | fields_to_plot (list): the fields indicated in the configuration that should be plotted for each pulse
        | width (int): the width of the plot, indicated in the configuration file
        | height (int): the height of the plot, indicated in the configuration file
    """
    figs = []

    if "B0 value" in fields_to_plot and pulse["s_globalQuantities_b0_value"]:
        figs.append(create_plot_bokeh("B0 value", pulse["s_globalQuantities_b0_value"], width, height))

    if "Power nbi" in fields_to_plot and pulse["s_heatingCurrentDrive_powerNbi_value"]:
        figs.append(create_plot_bokeh("Power nbi", pulse["s_heatingCurrentDrive_powerNbi_value"], width, height))

    if "Neutron Fluxes" in fields_to_plot and pulse["s_fusion_neutronFluxes_total_value"]:
        figs.append(create_plot_bokeh("Neutron Fluxes", pulse["s_fusion_neutronFluxes_total_value"], width, height))

    if "Beta Pol value" in fields_to_plot and pulse["s_globalQuantities_betapol_value"]:
        figs.append(create_plot_bokeh("Beta Pol value", pulse["s_globalQuantities_betapol_value"], width, height))

    if "Beta Tor value" in fields_to_plot and pulse["s_globalQuantities_betaTor_value"]:
        figs.append(create_plot_bokeh("Beta Tor value", pulse["s_globalQuantities_betaTor_value"], width, height))

    if "Current Ohm value" in fields_to_plot and pulse["s_globalQuantities_currentOhm_value"]:
        figs.append(create_plot_bokeh("Current Ohm value", pulse["s_globalQuantities_currentOhm_value"], width, height))

    if "Energy BField Pol value" in fields_to_plot and pulse["s_globalQuantities_energyBFieldPol_value"]:
        figs.append(create_plot_bokeh("Energy BField Pol value", pulse["s_globalQuantities_energyBFieldPol_value"],
                                      width, height))

    if "Energy Diamagnetic value" in fields_to_plot and pulse["s_globalQuantities_energyDiamagnetic_value"]:
        figs.append(create_plot_bokeh("Energy Diamagnetic value", pulse["s_globalQuantities_energyDiamagnetic_value"],
                                      width, height))

    if "Energy Total value" in fields_to_plot and pulse["s_globalQuantities_energyTotal_value"]:
        figs.append(create_plot_bokeh("Energy Total value",
                                      pulse["s_globalQuantities_energyTotal_value"], width, height))

    if "S Time Width" in fields_to_plot and pulse["s_timeWidth"]:
        figs.append(create_plot_bokeh("S Time Width", pulse["s_timeWidth"], width, height))

    if "S Time" in fields_to_plot and pulse["s_time"]:
        figs.append(create_plot_bokeh("S Time", pulse["s_time"], width, height))

    if "Ip value" in fields_to_plot and pulse["s_globalQuantities_ip_value"]:
        figs.append(create_plot_bokeh("Ip value", pulse["s_globalQuantities_ip_value"], width, height))

    if "Li value" in fields_to_plot and pulse["s_globalQuantities_li_value"]:
        figs.append(create_plot_bokeh("Li value", pulse["s_globalQuantities_li_value"], width, height))

    if "Tau Energy value" in fields_to_plot and pulse["s_globalQuantities_tauEnergy_value"]:
        figs.append(create_plot_bokeh("Tau Energy value", pulse["s_globalQuantities_tauEnergy_value"], width, height))

    if "Volume value" in fields_to_plot and pulse["s_globalQuantities_volume_value"]:
        figs.append(create_plot_bokeh("Volume value", pulse["s_globalQuantities_volume_value"], width, height))

    if "VLoop_value" in fields_to_plot and pulse["s_globalQuantities_vLoop_value"]:
        figs.append(create_plot_bokeh("VLoop_value", pulse["s_globalQuantities_vLoop_value"], width, height))

    if "Volume Average NE value" in fields_to_plot and pulse["s_volumeAverage_nE_value"]:
        figs.append(
            create_plot_bokeh("Volume Average NE value", pulse["s_volumeAverage_nE_value"], width, height))
    return figs


def visualize_multiple_pulses(pulses, fields_to_plot, width, height,multi=False):
    """
    Based on the configuration parameters, read on startup, the demonstrator decides which pulse fields of JSON type
    will plot in the DetailView

    Args
        | pulse (Pulse): the list of pulses selected to be shown in the detailed view
        | fields_to_plot (list): the fields indicated in the configuration that should be plotted for each pulse
        | width (int): the width of the plot, indicated in the configuration file
        | height (int): the height of the plot, indicated in the configuration file
    """
    figs = []

    if "B0 value" in fields_to_plot:
        values = []
        for pulse in pulses:
            values.append(pulse["s_globalQuantities_b0_value"])
        figs.append(create_multiple_plot("B0 value", values, width, height))
    
    if "Ip value" in fields_to_plot and multi == True:
        values = []
        for pulse in pulses:
            values.append(pulse["s_globalQuantities_ip_value"])
        figs.append(create_multiple_plot("Ip value", values, width, height))


    return figs


def create_plot_bokeh(parameter, data, width, height):
    """
    Function that generates the plots on runtime for a specific pulse using the bokeh framework

    Args
        | parameter (str): the name of the field that is plotted
        | data (json): the JSON field with the time series
        | width (int): the width of the plot initialized in the configuration
        | height (int): the height of the plot initialized in the configuration

    Returns
        | dict: dictionary containing the div and javascript code generated by bokeh
    """
    x = np.array(data["times"])
    y = np.array(data["values"])
    plot = figure(title=parameter,
                  x_axis_label="Times",
                  y_axis_label="Values",
                  plot_width=width,
                  plot_height=height)
    plot.line(x, y, line_width=2)
    script, div = components(plot)
    fig = {"script": script, "div": div, "title": parameter}
    return fig


def create_multiple_plot(parameter, data, width, height):
    """
    Function that generates the plots on runtime for a specific set of pulses using the bokeh framework

    Args
        | parameter (str): the name of the field that is plotted
        | data (json): the JSON field with the time series of all plots
        | width (int): the width of the plot initialized in the configuration
        | height (int): the height of the plot initialized in the configuration

    Returns
        | dict: dictionary containing the div and javascript code generated by bokeh
    """

  
    xs = []
    for i in range(0,len(data)):
        xs.append(data[i]["times"])

    ys = []
    for i in range(0,len(data)):
        ys.append(data[i]["values"])

    logger.error(xs)
    logger.error(ys)

    # xs=[[1, 2, 3], [15, 13, 14]] 
    # ys=[[6, 7, 2], [4, 5, 7]]
                    

    plot = figure(title=parameter,
                  x_axis_label="Times",
                  y_axis_label="Values",
                  plot_width=width,
                  plot_height=height)
    plot.multi_line(xs, ys, line_width=2,color=['red','green','blue','black','orange'])
    script, div = components(plot)
    fig = {"script": script, "div": div, "title": parameter}
    return fig


def is_token_valid(request):
    """
    Function which checks if the token is expired. Performs an API call in the backend which uses dare-login with
    keycloak as backend to check the tokens.

    Args
        | request (WSGIRequest): the request containing the session

    Returns
        | bool: if the token is valid
    """
    token_issued = request.session["issued"]
    expires_in = request.session["expires_in"]
    exp_time = token_issued + expires_in
    refresh_token = request.session["refresh_token"]
    issuer = request.session["issuer"]
    time_now = int(time())
    if time_now >= exp_time:
        refresh_resp = requests.post(API_BASE_URL + "/accounts/refresh-token/",
                                     data={"refresh_token": refresh_token, "issuer": issuer})
        if refresh_resp.status_code == 200:
            response = json.loads(refresh_resp.text)
            request.session["access_token"] = response["access_token"]
            request.session["refresh_token"] = response["refresh_token"]
            request.session["expires_in"] = response["expires_in"]
            request.session["issued"] = int(time())
            return True
        else:
            return False
    else:
        return True


def cleanup_session(request):
    """
    Function to cleanup a user's session on logout or when a token is no longer valid and cannot be updated because
    the refresh token is also expired.

    Args
        | request (WSGIRequest): the request containing the session
    """
    if "username" in request.session.keys():
        del request.session["username"]
        del request.session["password"]
        del request.session["access_token"]
        del request.session["refresh_token"]
        del request.session["expires_in"]
        del request.session["issued"]
        del request.session["user_id"]
        del request.session["issuer"]
        del request.session["is_admin"]
        current_keys = list(request.session.keys())
        if current_keys:
            for key in current_keys:
                if key.startswith("shot"):
                    del request.session[key]
        request.session.modified = True


def submit_d4p(impl_id, pckg, workspace_id, pe_name, n_nodes, token, hostname, image=None,
               reqs=None, **kw):
    """
    Uses the Execution API to execute a dispel4py workflow

    Args
        | impl_id (int): the ID of the PEImpl to be executed
        | pckg (str): the name of the PEImpl's package
        | workspace_id (int): the ID of the relevant workspace
        | pe_name (str): the name of the PE
        | n_nodes (int): the number of the containers to be created
        | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
        | token (str): the access token aquired from the login function
        | image (str): the tag of the image of the container. If it's not provided the latest tag is used. The base tag
        is the registry.gitlab.com/project-dare/dare-platform/exec-context-d4p and we add the tag at the end, e.g.
        registry.gitlab.com/project-dare/dare-platform/exec-context-d4p:latest
        | reqs (str): URL to a requirements txt file for the execution

    """
    # Prepare data for posting
    data = {
        "impl_id": impl_id,
        "pckg": pckg,
        "wrkspce_id": workspace_id,
        "name": pe_name,
        "n_nodes": n_nodes,
        "access_token": token,
        "reqs": reqs if not (reqs is None) else "None"
    }
    if image:
        data["image"] = image
    d4p_args = {}
    for k in kw:
        d4p_args[k] = kw.get(k)
    data['d4p_args'] = json.dumps(d4p_args)
    # Request for dare api
    _r = requests.post(hostname + '/run-d4p', data=data)
    # Progress check
    if _r.status_code == 200:
        response = json.loads(_r.text)
        return response["run_dir"]
    else:
        print('DARE api resource / d4p-mpi-spec returns status_code: \
                ' + str(_r.status_code))
        print(_r.text)


def submit_cwl(hostname, token, workflow_name, workflow_version, shot, input_data=None, nodes=None):
    """
        Uses the Execution API to execute a CWL workflow

        Args
            | hostname (str): the base URL to the registry (e.g. https://testbed.project-dare.eu/d4p-registry)
            | token (str): the access token aquired from the login function
            | workflow_name (str): the name of the registered workflow to be executed
            | workflow_version (str): the version of the workflow to be executed
            | input_data (dict): a dictionary with the necessary input data
            | nodes (int): the number of the containers to be created
        """
    url = hostname + "/run-cwl"

    data = {
        "access_token": token,
        "workflow_name": workflow_name,
        "workflow_version": workflow_version,
        "add_pod_name": "pulse-{}".format(shot)
    }

    if input_data:
        data["input_data"] = json.dumps(input_data)
    if nodes:
        data["nodes"] = nodes

    response = requests.post(url, data=data,verify=false)
    if response.status_code == 200:
        response = json.loads(response.text)
        return response["run_dir"]
    else:
        print('DARE api resource / cwl-mpi-spec returns status_code: \
                ' + str(response.status_code))
        print(response.text)


def get_file_content(token, hostname, run_dir, filename):
    url = hostname + "/file-content"
    params = {
        "access_token": token,
        "run_dir": run_dir,
        "filename": filename
    }
    response = requests.get(url, params=params)
    while response.text == "File is not created yet":
        response = requests.get(url, params=params)
    return response.status_code, response.text
