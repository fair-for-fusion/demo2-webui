(function() {
    var filterSections = document.getElementsByClassName("filter-section");
    for (var i = 0; i < filterSections.length; i++) {
        filterSections[i].addEventListener("click", function() {
            this.classList.toggle("filter-active");
            var content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }
        });
    }

    var rows = document.getElementsByClassName("expandable-row");
    for (var i = 0; i < rows.length; i++) {
        rows[i].firstElementChild.addEventListener("click", function() {
            var content = this.parentElement.nextElementSibling;
            this.parentElement.classList.toggle("active");
            if (content.style.display !== "") {
                content.style.display = "";
            } else {
                content.style.display = "table-row";
            }
        });
    }
})();
