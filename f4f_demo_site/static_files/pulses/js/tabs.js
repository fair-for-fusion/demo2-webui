$(document).ready(function () {
    let annotations_updated = localStorage.getItem("annotations_updated")
    if (annotations_updated === "true") {
        $('ul.tabs-pulse li').removeClass('current-pulse');
        $('.tab-content-pulse').removeClass('current-pulse');
        $("#annotations-tab").addClass('current-pulse');
        $("#Annotations-detail").addClass('current-pulse');
        let msg_containers = document.getElementById("msg-containers")
        msg_containers.scrollTop = msg_containers.scrollHeight;
        localStorage.setItem("annotations_updated", "false");
    }

    let pids_updated = localStorage.getItem("pids_updated")
    if (pids_updated === "true") {
       $('ul.tabs-pulse li').removeClass('current-pulse');
        $('.tab-content-pulse').removeClass('current-pulse');
        $("#pid-tab").addClass('current-pulse');
        $("#PID").addClass('current-pulse');
        let pid_container = document.getElementById("pid-container")
        pid_container.scrollTop = pid_container.scrollHeight;
        localStorage.setItem("pids_updated", "false");
    }

    $('ul.tabs-pulse li').click(function () {
        let tab_id = $(this).attr('data-tab');
        $('ul.tabs-pulse li').removeClass('current-pulse');
        $('.tab-content-pulse').removeClass('current-pulse');
        $(this).addClass('current-pulse');
        $("#" + tab_id).addClass('current-pulse');
        if (tab_id === "Annotations-detail") {
            let msg_containers = document.getElementById("msg-containers")
            msg_containers.scrollTop = msg_containers.scrollHeight;
        }
    });
});