function getUrlParameter(sParam) {
    let sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            if (sParameterName.length > 2) {
                let finalValue = sParameterName[1]
                for (let j=2; j < sParameterName.length; j++) {
                    finalValue = finalValue + "=" + sParameterName[j]
                }
                return finalValue === undefined ? true : decodeURIComponent(finalValue)
            }
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}