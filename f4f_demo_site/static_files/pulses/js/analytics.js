function fill_analytics(data) {
    $.each(data, function (key, value) {
        console.log(value);
        if (key !== "shot") {
            document.getElementById(key + "_nobs").innerHTML = value["nobs"];
            document.getElementById(key + "_minmax").innerHTML = value["minmax"];
            document.getElementById(key + "_mean").innerHTML = value["mean"];
            document.getElementById(key + "_variance").innerHTML = value["variance"];
            document.getElementById(key + "_skewness").innerHTML = value["skewness"];
            document.getElementById(key + "_kurtosis").innerHTML = value["kurtosis"];
        }
    });
}

function get_file_content(shot, access_token, run_dir, out_dir, filename) {
    let url = "/api/pulses-full/analytics" + "?access_token=" + access_token + "&run_dir=" + run_dir +
        "&out_dir=" + out_dir + "&filename=" + filename + "&shot=" + shot;
    $.get(url).done(function (data) {
        console.log(data);
        if (data === "File is not created yet") {
            document.getElementById('no-refresh').style.display = "block";
            document.getElementById('no-refresh').innerHTML = "Do not refresh the page. Calculating analytics...";
            get_file_content(shot, access_token, run_dir, out_dir, filename);
        } else {
            $.each(JSON.parse(data), function (key, value) {
                console.log(value);
                if (key !== "shot") {
                    document.getElementById(key + "_nobs").innerHTML = value["nobs"];
                    document.getElementById(key + "_minmax").innerHTML = value["minmax"];
                    document.getElementById(key + "_mean").innerHTML = value["mean"];
                    document.getElementById(key + "_variance").innerHTML = value["variance"];
                    document.getElementById(key + "_skewness").innerHTML = value["skewness"];
                    document.getElementById(key + "_kurtosis").innerHTML = value["kurtosis"];
                }
                document.getElementById('no-refresh').style.display = "none";
                let session_update_url = "/ui/update-session/";
                $.post(session_update_url, data).done(function (response) {
                    console.log(response);
                }).fail(function (jqXHR) {
                    console.log("Error: " + jqXHR.responseText);
                });
            });

        }
    }).fail(function (jqXHR) {
        console.log(jqXHR.response.text);
    });
}
