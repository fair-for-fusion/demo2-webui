function showDiv() {
    document.getElementById("plot-overlay").style.display = "block";
    let selections = $('#selector option:selected').toArray()
    let row = document.getElementById("plot-table");
    let cells = row.childNodes;
    for (let i = 0; i < cells.length; i++) {
        let other_cell = cells[i].id;
        if (other_cell != null) {
            let actual_cell = document.getElementById(other_cell);
            if (actual_cell != null) {
                actual_cell.style.visibility = "hidden";
                actual_cell.style.display = "none";
            }
        }
    }
    if (selections != null) {
        if (selections.length >= 3) {
            document.getElementById("plot-overlay").style.marginTop = "-150px";
        } else {
            document.getElementById("plot-overlay").style.marginTop = "0px";
        }
        for (let i = 0; i < selections.length; i++) {
            let selection = selections[i].text
            let cell = document.getElementById(selection);
            cell.style.display = "block";
            cell.style.visibility = "visible";
        }
    }
}

function showDivMultiple() {
    document.getElementById("multi-plot-overlay").style.display = "block";
    let selections = $('#selector option:selected').toArray()
    let row = document.getElementById("multi-plot-table");
    let cells = row.childNodes;
    for (let i = 0; i < cells.length; i++) {
        let other_cell = cells[i].id;
        if (other_cell != null) {
            let actual_cell = document.getElementById(other_cell);
            if (actual_cell != null) {
                actual_cell.style.visibility = "hidden";
                actual_cell.style.display = "none";
            }
        }
    }
    if (selections != null) {
        if (selections.length >= 3) {
            document.getElementById("multi-plot-overlay").style.marginTop = "-150px";
        } else {
            document.getElementById("multi-plot-overlay").style.marginTop = "0px";
        }
        for (let i = 0; i < selections.length; i++) {
            let selection = selections[i].text
            let cell = document.getElementById(selection);
            cell.style.display = "block";
            cell.style.visibility = "visible";
        }
    }
}