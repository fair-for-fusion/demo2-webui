from datetime import datetime
from os import getcwd, mkdir
from os.path import join, exists

import numpy as np
import requests
from bokeh.embed import components
from bokeh.plotting import figure

from demo.models import *
# from f4f_demo_site.settings import LOGIN_URL


# def auth_user(username, password, issuer):
#     """
#     Authenticates a user in the dare-login (uses Keycloak as backend)

#     Args
#         | username (str): the username of the user to be authenticated
#         | password (str): the password of the user to be authenticated
#         | issuer (str): the issuer service, uses dare as default but 3rd party identity providers can be enabled

#     Returns
#         | response: if the response is successful the token, refresh token and expiration time are returned, otherwise
#         the function returns the error message
#     """
#     url = LOGIN_URL + "/auth"
#     response = requests.post(url, data=json.dumps(
#         {"username": username, "password": password, "requested_issuer": issuer}))
#     if response.status_code == 200:
#         token = json.loads(response.text)["access_token"]
#         ref_token = json.loads(response.text)["refresh_token"]
#         expires_in = json.loads(response.text)["expires_in"]
#         return token, ref_token, expires_in
#     else:
#         return response.text


# def register_user(username, email, first_name, last_name, password, register=True):
#     url = LOGIN_URL + "/register" if register else LOGIN_URL + "/update-user"
#     data = {
#         "username": username,
#         "password": password,
#         "email": email,
#         "first_name": first_name,
#         "last_name": last_name
#     }
#     response = requests.post(url, data=json.dumps(data))
#     return response.status_code, response.text


# def validate_token(token, full_resp=True):
#     """
#     Validates a token provided by the user in a request. It uses the dare-login service (with keycloak in the backend).

#     Args
#         | token (str): the access token of the user provided in the request
#         | full_resp (bool): if set to true the dare-login returns also the user data (e.g. name, last name etc),
#         otherwise returns only if the token is valid

#     Returns
#         response: the message of the response
#     """
#     url = LOGIN_URL + "/validate-token"
#     response = requests.post(url, data=json.dumps({"access_token": token, "full_resp": full_resp}))
#     return json.loads(response.text) if response.status_code == 200 else response.text


# def refresh_token(ref_token, issuer):
#     url = LOGIN_URL + "/refresh-token"
#     response = requests.post(url, data=json.dumps({"refresh_token": ref_token, "issuer": issuer}))
#     return response


def check_passwords_match(current_password, password, repeat_password, user):
    if current_password and password and repeat_password:
        if user.check_password(current_password):
            if password == repeat_password:
                if user.check_password(current_password):
                    return "You have used the same password"
                else:
                    user.password = password
            else:
                return "Password and repeat password do not match"
        else:
            return "The current password is not correct!"
    elif not current_password and not password and not repeat_password:
        return True
    else:
        return "You should provide the current password, the new password (twice)"


def temp_store_data_files(files):
    """
    Function used by the create pulses API. It stores the uploaded files in a temporary directory so as to be accessed
    by the celery job. Once the job is finished and the data stored in the MySQL DB, the folder is deleted.

    Args
        | files (dict): a dictionary of files associated with a machine
    """
    if files:
        if not exists(join(getcwd(), "temp")):
            mkdir(join(getcwd(), "temp"))
        for machine_dict in files:
            for machine, csvs in machine_dict.items():
                if not exists(join(getcwd(), "temp", machine)):
                    mkdir(join(getcwd(), "temp", machine))
                for name, content in csvs.items():
                    with open(join(getcwd(), "temp", machine, name), "w") as f:
                        f.write(content)


def visualize_parameters(pulse, fields_to_plot, width, height):
    """
    Based on the configuration parameters, read on startup, the demonstrator decides which pulse fields of JSON type
    will plot in the DetailView

    Args
        | pulse (Pulse): the pulse object selected to be shown in the detailed view
        | fields_to_plot (list): the fields indicated in the configuration that should be plotted for each pulse
        | width (int): the width of the plot, indicated in the configuration file
        | height (int): the height of the plot, indicated in the configuration file
    """
    figs = []

    if "B0 value" in fields_to_plot and pulse.s_globalQuantities_b0_value:
        figs.append(create_plot_bokeh("B0 value", pulse.s_globalQuantities_b0_value, width, height))

    if "Power nbi" in fields_to_plot and pulse.s_heatingCurrentDrive_powerNbi_value:
        figs.append(create_plot_bokeh("Power nbi", pulse.s_heatingCurrentDrive_powerNbi_value, width, height))

    if "Neutron Fluxes" in fields_to_plot and pulse.s_fusion_neutronFluxes_total_value:
        figs.append(create_plot_bokeh("Neutron Fluxes", pulse.s_fusion_neutronFluxes_total_value, width, height))

    if "Beta Pol value" in fields_to_plot and pulse.s_globalQuantities_betapol_value:
        figs.append(create_plot_bokeh("Beta Pol value", pulse.s_globalQuantities_betapol_value, width, height))

    if "Beta Tor value" in fields_to_plot and pulse.s_globalQuantities_betaTor_value:
        figs.append(create_plot_bokeh("Beta Tor value", pulse.s_globalQuantities_betaTor_value, width, height))

    if "Current Ohm value" in fields_to_plot and pulse.s_globalQuantities_currentOhm_value:
        figs.append(create_plot_bokeh("Current Ohm value", pulse.s_globalQuantities_currentOhm_value, width, height))

    if "Energy BField Pol value" in fields_to_plot and pulse.s_globalQuantities_energyBFieldPol_value:
        figs.append(create_plot_bokeh("Energy BField Pol value", pulse.s_globalQuantities_energyBFieldPol_value,
                                      width, height))

    if "Energy Diamagnetic value" in fields_to_plot and pulse.s_globalQuantities_energyDiamagnetic_value:
        figs.append(create_plot_bokeh("Energy Diamagnetic value", pulse.s_globalQuantities_energyDiamagnetic_value,
                                      width, height))

    if "Energy Total value" in fields_to_plot and pulse.s_globalQuantities_energyTotal_value:
        figs.append(create_plot_bokeh("Energy Total value", pulse.s_globalQuantities_energyTotal_value, width, height))

    if "S Time Width" in fields_to_plot and pulse.s_timeWidth:
        figs.append(create_plot_bokeh("S Time Width", pulse.s_timeWidth, width, height))

    if "S Time" in fields_to_plot and pulse.s_time:
        figs.append(create_plot_bokeh("S Time", pulse.s_time, width, height))

    if "Ip value" in fields_to_plot and pulse.s_globalQuantities_ip_value:
        figs.append(create_plot_bokeh("Ip value", pulse.s_globalQuantities_ip_value, width, height))

    if "Li value" in fields_to_plot and pulse.s_globalQuantities_li_value:
        figs.append(create_plot_bokeh("Li value", pulse.s_globalQuantities_li_value, width, height))

    if "Tau Energy value" in fields_to_plot and pulse.s_globalQuantities_tauEnergy_value:
        figs.append(create_plot_bokeh("Tau Energy value", pulse.s_globalQuantities_tauEnergy_value, width, height))

    if "Volume value" in fields_to_plot and pulse.s_globalQuantities_volume_value:
        figs.append(create_plot_bokeh("Volume value", pulse.s_globalQuantities_volume_value, width, height))

    if "VLoop_value" in fields_to_plot and pulse.s_globalQuantities_vLoop_value:
        figs.append(create_plot_bokeh("VLoop_value", pulse.s_globalQuantities_vLoop_value, width, height))

    if "Volume Average NE value" in fields_to_plot and pulse.s_volumeAverage_nE_value:
        figs.append(create_plot_bokeh("Volume Average NE value", pulse.s_volumeAverage_nE_value, width, height))
    return figs


def create_plot_bokeh(parameter, data, width, height):
    """
    Function that generates the plots on runtime for a specific pulse using the bokeh framework

    Args
        | shot (int): the shot of the pulse
        | number (int): the number of the pulse
        | parameter (str): the name of the field that is plotted
        | data (json): the JSON field with the time series
        | width (int): the width of the plot initialized in the configuration
        | height (int): the height of the plot initialized in the configuration

    Returns
        | dict: dictionary containing the div and javascript code generated by bokeh
    """
    x = np.array(data["times"])
    y = np.array(data["values"])
    plot = figure(title=parameter,
                  x_axis_label="Times",
                  y_axis_label="Values",
                  plot_width=width,
                  plot_height=height)
    plot.line(x, y, line_width=2)
    script, div = components(plot)
    fig = {"script": script, "div": div, "title": parameter}
    return fig


def get_pulses(search_txt, start_date, end_date, machine, owner, shot_number, pid_id):
    """
    Function to get the pulse queryset based on the filter options (if they are given)

    Args
        | search_txt (str): free text on the annotations and PIDs descriptions and page URLs
        | start_data (str): the start date to search the pulses (on creation date field)
        | end_date (str): the end date to query the pulses (on creation date field)
        | machine (str): the name of the associated experiment
        | owner (str): the username of the user that stored the pulse
        | shot_number (int): the shot number of the pulse
        | pid_id (str): the PID number

    Returns
        | queryset: the pulse results
    """
    # preprocess input
    if start_date:
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
    if end_date:
        end_date = datetime.strptime(end_date, "%Y-%m-%d")
    if machine == "Any":
        machine = ""
    if machine:
        machine = Experiment.objects.get(short_name=machine)
    if owner == "Any":
        owner = ""
    if owner:
        owner = User.objects.get(username=owner)
    if pid_id == "undefined":
        pid_id = None
    if shot_number == "undefined":
        shot_number = None

    # all fields

    if search_txt and start_date and end_date and machine and owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, experiment=machine, owner=owner,
                                      s_idsProperties_creationDate__range=[start_date, end_date])
    # four fields

    elif search_txt and start_date and end_date and machine and not owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, experiment=machine,
                                      s_idsProperties_creationDate__range=[start_date, end_date])
    elif search_txt and start_date and end_date and not machine and owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, owner=owner,
                                      s_idsProperties_creationDate__range=[start_date, end_date])
    elif search_txt and start_date and not end_date and machine and owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, experiment=machine, owner=owner,
                                      s_idsProperties_creationDate__gte=start_date)
    elif search_txt and not start_date and end_date and machine and owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, owner=owner, experiment=machine,
                                      s_idsProperties_creationDate__lte=end_date)
    elif not search_txt and start_date and end_date and machine and owner:
        pulses = Pulse.objects.filter(s_idsProperties_creationDate__range=[start_date, end_date],
                                      experiment=machine, owner=owner)
    # three fields

    elif search_txt and start_date and end_date and not machine and not owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt,
                                      s_idsProperties_creationDate__range=[start_date, end_date])
    elif search_txt and start_date and not end_date and machine and not owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, experiment=machine,
                                      s_idsProperties_creationDate__gte=start_date)
    elif search_txt and not start_date and end_date and machine and not owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, experiment=machine,
                                      s_idsProperties_creationDate__lte=end_date)
    elif not search_txt and start_date and end_date and machine and not owner:
        pulses = Pulse.objects.filter(experiment=machine, s_idsProperties_creationDate__range=[start_date, end_date])
    elif search_txt and start_date and not end_date and not machine and owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, owner=owner,
                                      s_idsProperties_creationDate__gte=start_date)
    elif search_txt and not start_date and end_date and not machine and owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, owner=owner,
                                      s_idsProperties_creationDate__lte=end_date)
    elif not search_txt and start_date and end_date and not machine and owner:
        pulses = Pulse.objects.filter(s_idsProperties_creationDate__range=[start_date, end_date],
                                      owner=owner)
    elif search_txt and not start_date and not end_date and machine and owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, experiment=machine, owner=owner)
    elif not search_txt and start_date and not end_date and machine and owner:
        pulses = Pulse.objects.filter(experiment=machine, owner=owner,
                                      s_idsProperties_creationDate__gte=start_date)
    elif not search_txt and not start_date and end_date and machine and owner:
        pulses = Pulse.objects.filter(experiment=machine, owner=owner,
                                      s_idsProperties_creationDate__lte=end_date)
    # two fields

    elif search_txt and start_date and not end_date and not machine and not owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, s_idsProperties_creationDate__gte=start_date)
    elif search_txt and not start_date and end_date and not machine and not owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, s_idsProperties_creationDate__lte=end_date)
    elif search_txt and not start_date and not end_date and machine and not owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, experiment=machine)
    elif search_txt and not start_date and not end_date and not machine and owner:
        pulses = Pulse.objects.filter(cannots__search=search_txt, owner=owner)
    elif not search_txt and start_date and end_date and not machine and not owner:
        pulses = Pulse.objects.filter(s_idsProperties_creationDate__range=[start_date, end_date])
    elif not search_txt and start_date and not end_date and machine and not owner:
        pulses = Pulse.objects.filter(s_idsProperties_creationDate__gte=start_date, experiment=machine)
    elif not search_txt and start_date and not end_date and not machine and owner:
        pulses = Pulse.objects.filter(s_idsProperties_creationDate__gte=start_date, owner=owner)
    elif not search_txt and not start_date and end_date and machine and not owner:
        pulses = Pulse.objects.filter(experiment=machine, s_idsProperties_creationDate__lte=end_date)
    elif not search_txt and not start_date and end_date and not machine and owner:
        pulses = Pulse.objects.filter(owner=owner, s_idsProperties_creationDate__lte=end_date)
    elif not search_txt and not start_date and not end_date and machine and owner:
        pulses = Pulse.objects.filter(owner=owner, experiment=machine)

    # one field

    elif search_txt and not start_date and not end_date and not machine and not owner and not pid_id and \
            not shot_number:
        pulses = Pulse.objects.filter(cannots__search=search_txt)
    elif not search_txt and start_date and not end_date and not machine and not owner and not pid_id and \
            not shot_number:
        pulses = Pulse.objects.filter(s_idsProperties_creationDate__gte=start_date)
    elif not search_txt and not start_date and end_date and not machine and not owner and not pid_id and \
            not shot_number:
        pulses = Pulse.objects.filter(s_idsProperties_creationDate__lte=end_date)
    elif not search_txt and not start_date and not end_date and machine and not owner and not pid_id and \
            not shot_number:
        pulses = Pulse.objects.filter(experiment=machine)
    elif not search_txt and not start_date and not end_date and not machine and owner and not pid_id and \
            not shot_number:
        pulses = Pulse.objects.filter(owner=owner)
    elif not search_txt and not start_date and not end_date and not machine and not owner and pid_id and \
            not shot_number:
        pulses = Pulse.objects.filter(pid__pid_number=pid_id)
    elif not search_txt and not start_date and not end_date and not machine and not owner and not pid_id and \
            shot_number:
        pulses = Pulse.objects.filter(shot=shot_number)
    # no fields

    else:
        pulses = Pulse.objects.all()
    return pulses
