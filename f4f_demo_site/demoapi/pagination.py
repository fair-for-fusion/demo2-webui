import logging
from collections import OrderedDict

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.utils.urls import remove_query_param, replace_query_param

from f4f_demo_site.settings import REST_FRAMEWORK

logger = logging.getLogger(__name__)


class F4fPagination(PageNumberPagination):
    """
    Class used to paginate the results from the DB
    """
    page_size = REST_FRAMEWORK["PAGE_SIZE"]

    def get_next_link(self):
        """
        Returns the next link without the domain to get the next data based on the page size
        """
        logger.debug("Adapting next link url by removing domain")
        if not self.page.has_next():
            return None
        url = self.request.get_full_path()
        page_number = self.page.next_page_number()
        return replace_query_param(url, self.page_query_param, page_number)

    def get_previous_link(self):
        """
        Returns the previous link without the domain to get the next data based on the page size
        """
        logger.debug("Adapting previous link url by removing domain")
        if not self.page.has_previous():
            return None
        url = self.request.get_full_path()
        page_number = self.page.previous_page_number()
        if page_number == 1:
            return remove_query_param(url, self.page_query_param)
        return replace_query_param(url, self.page_query_param, page_number)

    def get_paginated_response(self, data):
        logger.debug("Creating paginated response")
        return Response(OrderedDict([
            ('lastPage', self.page.paginator.count),
            ('countItemsOnPage', self.page_size),
            ('current', self.page.number),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))
