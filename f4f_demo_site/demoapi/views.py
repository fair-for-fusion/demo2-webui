import logging

import requests
from django.core.exceptions import EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned
from django.db.utils import IntegrityError
from django.http import JsonResponse, HttpResponse
from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt

from demoapi import utils, tasks
from demoapi.pagination import F4fPagination
from f4f_demo_site import settings
from f4f_demo_site.settings import properties, EXEC_API_URL
from .permissions import IsAuthenticated
from .serializers import *

logger = logging.getLogger(__name__)


# *********************************** Experiments *********************************************** #
class ExperimentViewSet(viewsets.ModelViewSet):
    """
    RESTful API view for the Experiment model
    """
    queryset = Experiment.objects.all()
    serializer_class = ExperimentSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """
        API endpoint to store a new machine in the DB
        """
        try:
            short_name = request.data["short_name"]
        except(KeyError, Exception):
            msg = "Missing machine name from request!"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        url = request.data.get("url")
        experiment = Experiment()
        experiment.short_name = short_name
        experiment.url = url
        try:
            experiment.save(username=request.data["username"])
            serializer = ExperimentSerializer(experiment, many=False, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(IntegrityError, Exception) as e:
            msg = "An error occurred while saving machine with name {}! {}".format(short_name, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["get"], detail=False, url_name="byname", url_path="bypath")
    def get_experiment_by_name(self, request, *args, **kwargs):
        """
        API endpoint to retrieve a machine by name.
        Usage: http://<IP>:<port>/api/experiments/byname            GET request

        Request params
            | name: the name of the requested machine / experiment

        Returns
            | response: the retrieved machine
        """
        try:
            experiment_name = request.query_params["name"]
        except (KeyError, Exception) as e:
            msg = "Provide the machine's name! {}".format(e)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            machine = Experiment.objects.get(short_name=experiment_name)
            serializer = ExperimentSerializer(machine, many=False, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving machine with name {}: {}".format(experiment_name, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ExperimentPagedViewSet(viewsets.ModelViewSet):
    """
    View for paged Experiment results
    """
    serializer_class = ExperimentSerializer
    pagination_class = F4fPagination
    paginate_by = settings.REST_FRAMEWORK["PAGE_SIZE"]
    ordering = ['id']
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        Machine paged results. Takes into account possible query parameters to filter the results
        """
        machine = self.request.query_params.get("machine")
        short_name = self.request.query_params.get("short_name")

        if machine:
            machines = Experiment.objects.filter(short_name=machine)
        elif short_name:
            machines = Experiment.objects.filter(short_name=short_name)
        else:
            machines = Experiment.objects.all()
        return machines


# *********************************** Annotations *********************************************** #
class AnnotationViewSet(viewsets.ModelViewSet):
    """
    RESTful View for the Annotation model
    """
    model = Annotation
    queryset = Annotation.objects.all()
    serializer_class = AnnotationSerializer
    lookup_field = "id"
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """
        API endpoint to store a new annotation
        """
        description = request.data["description"]
        username = request.data["username"]
        pulse_id = int(request.data["pk"])
        pulse = Pulse.objects.get(id=pulse_id)
        user = User.objects.get(username=username)
        annotation = Annotation()
        annotation.text = description
        annotation.pulse = pulse
        annotation.user = user
        annotation.time = timezone.now()

        try:
            annotation.save(username=username)
            serializer = AnnotationSerializer(annotation, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except (IntegrityError, Exception) as e:
            msg = "Something went wrong while saving the new annotation! {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="bypulse", url_path="bypulse")
    def get_annotations_by_pulse(self, request, *args, **kwargs):
        """
        API endpoint to retrieve all the annotations related to a specific pulse.
        Usage: https://<ip>:<port>/api/annotations/bypulse    GET request

        Request params
            | pulse_id: the id of the specific pulse

        Returns
            | response: list of annotations for the requested pulse
        """
        try:
            pulse_id = request.query_params["pulse_id"]
            logger.debug("Getting annotations for pulse with id {}".format(pulse_id))
        except KeyError:
            msg = "You should provide the pulse id"
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        sort = request.query_params["sort"] if "sort" in request.query_params.keys() else False
        try:
            pulse = Pulse.objects.get(id=pulse_id)
            annotations = Annotation.objects.filter(pulse=pulse).order_by("time") if sort else \
                Annotation.objects.filter(pulse=pulse)
            serializer = AnnotationSerializer(annotations, many=True, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving annotations for pulse with id {}: {}".format(id, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="byuser", url_path="byuser")
    def get_annotations_by_user(self, request, *args, **kwargs):
        """
        API endpoint to get all the annotations written by a specific user
        """
        try:
            username = request.query_params["username"]
        except (KeyError, Exception):
            msg = "Missing username from the request"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            annotations = Annotation.objects.filter(user__username=username)
            serializer = AnnotationSerializer(annotations, many=True, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving annotations for pulse with id {}: {}".format(id, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class AnnotationsPagedViewSet(viewsets.ModelViewSet):
    """
    View for paged annotations
    """
    model = Annotation
    serializer_class = AnnotationSerializer
    lookup_field = "id"
    pagination_class = F4fPagination
    paginate_by = settings.REST_FRAMEWORK["PAGE_SIZE"]
    ordering = ['id']
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        Function to get annotation results in paged format (takes into account any given parameters)
        """
        username = self.request.query_params.get("username")
        shot = self.request.query_params.get("shot")
        number = self.request.query_params.get("number")
        pulse_id = self.request.query_params.get("pulse_id")
        user = None
        pulse = None

        if shot and number:
            pulse = Pulse.objects.get(shot=shot, number=number)
        elif pulse_id:
            pulse = Pulse.objects.get(id=pulse_id)
        if username:
            user = User.objects.get(username=username)

        if pulse and user:
            annotations = Annotation.objects.filter(user=user, pulse=pulse).order_by('-time')
        elif user and not pulse:
            annotations = Annotation.objects.filter(user=user).order_by('-time')
        elif pulse and not user:
            annotations = Annotation.objects.filter(pulse=pulse)
        else:
            annotations = Annotation.objects.all()
        return annotations


# *********************************** Users *********************************************** #
# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    """
    API view for Django user model
    """
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = "id"
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        try:
            username = request.data["new_username"]
            password = request.data["password"]
            confirm_password = request.data["confirm_password"]
            first_name = request.data["first_name"]
            last_name = request.data["last_name"]
            email = request.data["email"]
            role = request.data["role"]
            role = True if role == "true" else False
            register = request.data["register"]
            register = True if register == "true" else False

            if password != confirm_password:
                msg = "Password and confirm password do not match!"
                return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except(KeyError, Exception):
            msg = "Missing request arguments!"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        user = User()
        user.first_name = first_name
        user.last_name = last_name
        user.is_active = True
        user.password = password
        user.username = username
        user.email = email
        user.is_superuser = role

        # try:
        #     utils.register_user(username=username, email=email, first_name=first_name, last_name=last_name,
        #                         password=password, register=register)
        #     user.save()
        # except(IntegrityError, Exception) as e:
        #     msg = "An error occurred while saving user with username {}! {}".format(username, e)
        #     logger.error(msg)
        #     return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["POST"], detail=False, url_name="edit", url_path="edit")
    def edit_user(self, request, *args, **kwargs):
        """
        API endpoint to update a user's profile
        """
        # TODO check how to update a user in Keycloak so as to be able to update the password
        username = self.request.data.get("username")
        # user_id = self.request.data.get("user_id")

        first_name = self.request.data.get("first_name")
        last_name = self.request.data.get("last_name")
        email = self.request.data.get("email")
        password = self.request.data.get("password")
        current_password = self.request.data.get("current_password")
        repeat_password = self.request.data.get("repeat_password")
        try:
            user = User.objects.get(username=username)
            if first_name:
                user.first_name = first_name
            if last_name:
                user.last_name = last_name
            if email:
                user.email = email
            check_pass = utils.check_passwords_match(current_password=current_password, password=password,
                                                     repeat_password=repeat_password, user=user)
            if type(check_pass) == str:
                return Response(check_pass, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            user.save()
            serializer = UserSerializer(user, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving user with username: {}! {}".format(username, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="byusername", url_path="byusername")
    def get_by_username(self, request, *args, **kwargs):
        """
        API endpoint to get a user's data based on his/her username
        """
        try:
            username = request.query_params["username"]
        except (KeyError, Exception):
            msg = "Username was missing from the request"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            user = User.objects.get(username=username)
            serializer = UserSerializer(user, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving user with username: {}! {}".format(username, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="data-distinct", url_path="data-distinct")
    def get_user_data_distinct(self, request, *args, **kwargs):
        """
        API endpoint to get all the usernames, first names, last names and emails distinct
        """
        try:
            usernames = list(User.objects.order_by().values_list('username', flat=True).distinct())
            first_names = list(User.objects.order_by().values_list('first_name', flat=True).distinct())
            last_names = list(User.objects.order_by().values_list('last_name', flat=True).distinct())
            emails = list(User.objects.order_by().values_list('email', flat=True).distinct())
            return JsonResponse(json.dumps(
                {"usernames": usernames, "first_names": first_names, "last_names": last_names, "emails": emails}),
                safe=False, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving issuers in PID table {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserPagedViewSet(viewsets.ModelViewSet):
    """
    View for paged user results
    """
    serializer_class = UserSerializer
    pagination_class = F4fPagination
    paginate_by = settings.REST_FRAMEWORK["PAGE_SIZE"]
    ordering = ['id']
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        Function to get a user queryset based on the request parameters if given, otherwise it returns all the users.
        """
        username = self.request.query_params.get("username")
        first_name = self.request.query_params.get("first_name")
        last_name = self.request.query_params.get("last_name")
        email = self.request.query_params.get("email")

        # all fields
        if username and first_name and last_name and email:
            users = User.objects.filter(username=username, first_name=first_name, last_name=last_name, email=email)
        # three fields
        elif username and first_name and last_name and not email:
            users = User.objects.filter(username=username, first_name=first_name, last_name=last_name)
        elif username and first_name and not last_name and email:
            users = User.objects.filter(username=username, first_name=first_name, email=email)
        elif username and not first_name and last_name and email:
            users = User.objects.filter(username=username, last_name=last_name, email=email)
        elif not username and first_name and last_name and email:
            users = User.objects.filter(first_name=first_name, last_name=last_name, email=email)

        # two fields
        elif username and first_name and not last_name and not email:
            users = User.objects.filter(username=username, first_name=first_name)
        elif username and not first_name and last_name and not email:
            users = User.objects.filter(username=username, last_name=last_name)
        elif username and not first_name and not last_name and email:
            users = User.objects.filter(username=username, email=email)
        elif not username and first_name and last_name and not email:
            users = User.objects.filter(first_name=first_name, last_name=last_name)
        elif not username and first_name and not last_name and email:
            users = User.objects.filter(first_name=first_name, email=email)
        elif not username and not first_name and last_name and email:
            users = User.objects.filter(last_name=last_name, email=email)

        # one field
        elif username and not first_name and not last_name and not email:
            users = User.objects.filter(username=username)
        elif not username and first_name and not last_name and not email:
            users = User.objects.filter(first_name=first_name)
        elif not username and not first_name and last_name and not email:
            users = User.objects.filter(last_name=last_name)
        elif not username and not first_name and not last_name and email:
            users = User.objects.filter(email=email)

        # return all results
        else:
            users = User.objects.all()
        return users


class LoginView(viewsets.GenericViewSet):
    permission_classes = (AllowAny,)
    serializer_class = UserSerializer
    
   
    @action(methods=["POST"], detail=False, url_name="login", url_path="login")
    def login(self, request, *args, **kwargs):
        """
        API endpoint used in login view. Uses the dare-login as backend
        """
        username = request.data["username"]
        password = request.data["password"]
        issuer = "test"
        # issuer = request.data["issuer"] if "issuer" in request.data.keys() else "dare"

        # response = utils.auth_user(username, password, issuer)
        # if type(response) == str:
        #     return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # token, refresh_token, expires_in = response
        # valid_resp = utils.validate_token(token)
        # if type(valid_resp) == str:
        #     return Response(response.text, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        # user_id = valid_resp["user_id"]
        # email = valid_resp["email"]
        # given_name = valid_resp["given_name"]
        # family_name = valid_resp["family_name"]
        # issuer = valid_resp["issuer"]

        token = "test"
        refresh_token = "test"
        expires_in = "test"
        user_id = "test"
        email = "test"
        given_name = "test"
        family_name = "test"
        issuer = "test"
        try:
            u = User.objects.get(username=username)
            logger.debug("User with username {} already exists!".format(u.username))
        except (IntegrityError, Exception):
            logger.debug("User {} not found! Creating user".format(username))
            try:
                u = User.objects.create_superuser(username=username, password=password, email=email,
                                                  first_name=given_name, last_name=family_name)
                logger.debug("User {} is saved".format(username))
            except (IntegrityError, Exception) as e:
                msg = "An error occurred while saving user {}".format(e)
                logger.error(msg)
                return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        u.last_login = timezone.now()
        u.save()
        return JsonResponse(json.dumps({"access_token": token, "refresh_token": refresh_token,
                                        "expires_in": expires_in, "user_id": user_id, "issuer": issuer,
                                        "is_admin": u.is_superuser}),
                            safe=False, status=status.HTTP_200_OK)

    @action(methods=["POST"], detail=False, url_name="refresh-token", url_path="refresh-token")
    def refresh_token(self, request, *args, **kwargs):
        """
        API endpoint to refresh a token
        """
        ref_token = request.data.get("refresh_token")
        issuer = request.data.get("issuer")
        # response = utils.refresh_token(ref_token=ref_token, issuer=issuer)
        # if response.status_code == 200:
        #     return JsonResponse(response.text, safe=False, status=status.HTTP_200_OK)
        # else:
        #     return JsonResponse(response.text, safe=False, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


# *********************************** Pulses *********************************************** #
class  PulseViewSet(viewsets.ModelViewSet):
    """
    API view for pulses used in UI's detail view
    """
    model = Pulse
    queryset = Pulse.objects.all()
    serializer_class = PulseSerializer
    lookup_field = "id"
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """
        API endpoint to populate the DB with new pulses.
        Usage: http://<IP>:<port>/api/pulses/           POST request

        Request data
            | files: list containing a single dictionary. The dictionary has the machine names as keys and for each
            machine there is an associated dictionary with two CSV files (summary and summary_times)
        """

        username = "test"
        files = request.data["files"]
        utils.temp_store_data_files(files)
        print("Done copying!")
        tasks.create_pulses(username)
        print("Done creating")
        return Response("Success", status=status.HTTP_200_OK)

    @action(methods=["GET"], detail=False, url_name="details", url_path="details")
    def get_details(self, request, *args, **kwargs):
        """
        API endpoint used in the DetailView of the demo_ui application. The endpoint retrieves a single pulse based
        on its id and returns all the available information.
        Usage: http://<IP>:<port>/api/pulses/details            GET request

        Request params
            | pulse_id: the id of the specific pulse

        Returns
            | response: the retrieved pulse entry
        """
        try:
            pulse_id = request.query_params["pulse_id"]
            logger.debug("Getting info aboud pulse with id {}".format(pulse_id))
        except KeyError:
            msg = "You should provide the pulse id"
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            pulse = Pulse.objects.get(id=pulse_id)
            serializer = PulseSerializer(pulse, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving pulse with id {}: {}".format(id, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="shot_details", url_path="shot_details")
    def get_shot_details(self, request, *args, **kwargs):
        """
        API endpoint used in the DetailView of the demo_ui application. The endpoint retrieves a single pulse based
        on its shot_number and returns all the available information.
        Usage: http://<IP>:<port>/api/pulses/shot_details            GET request

        Request params
            | pulse_id: the id of the specific pulse

        Returns
            | response: the retrieved pulse entry
        """
        try:
            shot_number = request.query_params["shot"]
            logger.debug("Getting info aboud pulse with id {}".format(shot_number))
        except KeyError:
            msg = "You should provide the pulse shot number"
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            pulse = Pulse.objects.get(shot=shot_number)
            serializer = PulseSerializer(pulse, many=False, context={'request': request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving pulse with shot {}: {}".format(id, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="analytics", url_path="analytics")
    def get_analytics(self, request, *args, **kwargs):
        try:
            access_token = request.query_params["access_token"]
            run_dir = request.query_params["run_dir"]
            out_dir = request.query_params["out_dir"]
            filename = request.query_params["filename"]
            shot = request.query_params["shot"]
        except(KeyError, Exception) as e:
            msg = "Missing parameters from request! {}".format(e)
            logger.error(msg)
            return HttpResponse(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        params = {
            "access_token": access_token,
            "run_dir": run_dir,
            "output_dir": out_dir,
            "filename": filename
        }
        url = EXEC_API_URL + "/file-content"
        response = requests.get(url=url, params=params)
        if response.text != "File is not created yet":
            try:
                resp = json.loads(response.text)
                resp["shot"] = shot
                resp = json.dumps(resp)
                return HttpResponse(resp, status=status.HTTP_200_OK)
            except(BaseException, Exception) as e:
                logger.error(e)
                return HttpResponse(response.text, status=response.status_code)
        else:
            return HttpResponse(response.text, status=response.status_code)

    @action(methods=["GET"], detail=False, url_name="plot", url_path="plot")
    def plot_variables(self, request, *args, **kwargs):
        """
        API endpoint to request plots for specific variables in a pulse
        """
        try:
            pulse_id = request.query_params["pulse_id"]
        except(KeyError, Exception):
            msg = "You should provide the pulse ID"
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            pulse = Pulse.objects.get(id=pulse_id)
            fields_to_plot = request.query_params["fields_to_plot"] if "fields_to_plot" in request.query_params.keys() \
                else properties["plot"]["fields_to_plot"]
            figs = utils.visualize_parameters(pulse=pulse, fields_to_plot=fields_to_plot,
                                              width=properties["plot"]["width"],
                                              height=properties["plot"]["height"])
            return JsonResponse({"plot_images": figs}, safe=False, status=status.HTTP_200_OK)
        except(BaseException, Exception) as e:
            msg = "Could not plot variables for pulse with id {}! {}".format(pulse_id, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="download", url_path="download")
    def download_pulses(self, request, *args, **kwargs):
        """
        API endpoint to download a filter result of pulses in JSON format
        """
        num_limit = properties["pulse_max_download"]
        search_txt = self.request.query_params.get("search-txt")
        start_date = self.request.query_params.get("start_date")
        end_date = self.request.query_params.get("end_date")
        machine = self.request.query_params.get("machine")
        owner = self.request.query_params.get("owner")
        pid_id = self.request.query_params.get("pid_id")
        shot_number = self.request.query_params.get("shot_number")
        pulses = utils.get_pulses(search_txt=search_txt, start_date=start_date, end_date=end_date, machine=machine,
                                  owner=owner, shot_number=shot_number, pid_id=pid_id)
        if pulses.count() > num_limit:
            msg = "You requested more than {} pulses!".format(num_limit)
            logger.critical(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            serializer = PulseSerializer(pulses, many=True, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=["POST"], detail=False, url_name="add_pid", url_path="add_pid")
    def add_pid_to_pulse(self, request, *args, **kwargs):
        try:
            pid_number = request.data["pid_number"]
            pulse_id = request.data["pulse_id"]
            username = request.data["username"]
        except (KeyError, Exception):
            msg = "Missing PID number or pulse ID from the request"
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            pulse = Pulse.objects.get(id=pulse_id)
            pid = Pid.objects.get(pid_number=pid_number)
            pulse.pid_set.add(pid)
            pulse.save(kwargs={"username": username})
            serializer = PulseSerializer(pulse, many=False, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except () as e:
            msg = "Could not save association for pulse with ID {} and PID with number {}! Error: {}".format(pulse_id,
                                                                                                             pid_number,
                                                                                                             e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PulseSearchViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    """
    RESTful API view for Pulse model with limited information, used in the UI's ListView
    """
    serializer_class = PulseSummarySerializer
    pagination_class = F4fPagination
    paginate_by = settings.REST_FRAMEWORK["PAGE_SIZE"]
    ordering = ['id']
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        Overrides the get_queryset function based on the requests parameters. If the user fills any field of the
        search form in the demonstrator's UI, the list of the pulses is filtered to return the DB entries that match
        the query. If none of the form's fields are filled, the function returns all the pulses that are available in
        the DB.

        All the results are paged, therefore, each time that the user navigates in the next page, the next paged result
        is returned. The list of the pulses is paginated using the PageNumberPagination class.

        Returns
            | queryset: the list of the retrieved pulses
        """
        search_txt = self.request.query_params.get("search_for")
        start_date = self.request.query_params.get("start_date")
        end_date = self.request.query_params.get("end_date")
        machine = self.request.query_params.get("machine")
        owner = self.request.query_params.get("owner")
        pid_id = self.request.query_params.get("pid_id")
        shot_number = self.request.query_params.get("shot_number")
        return utils.get_pulses(search_txt, start_date, end_date, machine, owner, shot_number, pid_id)


# *********************************** PIDs *********************************************** #
class PidViewSet(viewsets.ModelViewSet):
    model = Pid
    queryset = Pid.objects.all()
    serializer_class = PidSerializer
    lookup_field = "id"
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        """
        API endpoint to store a new PID
        """
        try:
            pid_number = request.data["pid_number"]
            issuer = request.data["pid_issuer"]
            description = request.data["description"]
            page_url = request.data["page_url"]
        except(KeyError, Exception) as e:
            msg = "Missing arguments: {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        pid = Pid()
        pid.pid_number = pid_number
        pid.issuer = issuer
        pid.description = description
        pid.page_url = page_url
        try:
            pid.save(kwargs={"username": request.data["username"]})
            serializer = PidSerializer(pid, many=False, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(IntegrityError, Exception) as e:
            msg = "Error while trying to save PID with number {}: {}".format(pid_number, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="bypulse", url_path="bypulse")
    def get_pids_by_pulse(self, request, *args, **kwargs):
        """
        API endpoint to get a list of PIDs associated with a specific pulse
        """
        try:
            pulse_id = request.query_params.get("pulse_id")
        except (KeyError, Exception):
            msg = "Missing pulse ID from request"
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            pids = Pid.objects.filter(pulses__id=pulse_id)
            serializer = PidSerializer(pids, many=True, context={"request": request})
            return Response(serializer.data, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving pids for pulse with ID {}: {}".format(pulse_id, e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(methods=["GET"], detail=False, url_name="issuers-distinct", url_path="issuers-distinct")
    def get_issuers_distinct(self, request, *args, **kwargs):
        """
        API endpoint to get a list of distinct issuers for PIDs
        """
        try:
            issuers = list(Pid.objects.order_by().values_list('issuer', flat=True).distinct())
            return JsonResponse(json.dumps({"issuers": issuers}), safe=False, status=status.HTTP_200_OK)
        except(EmptyResultSet, ObjectDoesNotExist, MultipleObjectsReturned, Exception) as e:
            msg = "Error while retrieving issuers in PID table {}".format(e)
            logger.error(msg)
            return Response(msg, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PidPagedViewSet(viewsets.ModelViewSet):
    """
    Class for paged PIDs
    """
    serializer_class = PidSerializer
    pagination_class = F4fPagination
    paginate_by = settings.REST_FRAMEWORK["PAGE_SIZE"]
    ordering = ['id']
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        Function to get a paged queryset of PIDs. It is filtered by the query params if given.
        """
        pid_number = self.request.query_params.get("pid_number")
        search_txt = self.request.query_params.get("search_txt")
        issuer = self.request.query_params.get("issuer")

        if pid_number:
            pids = Pid.objects.filter(pid_number=pid_number)
        else:
            if search_txt and issuer:
                pids = Pid.objects.filter(search_description__search=search_txt, issuer=issuer)
            elif search_txt and not issuer:
                pids = Pid.objects.filter(search_description__search=search_txt)
            elif not search_txt and issuer:
                pids = Pid.objects.filter(issuer=issuer)
            else:
                pids = Pid.objects.all()
        return pids
