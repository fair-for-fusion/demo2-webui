from django.conf.urls import include, url
from rest_framework import routers

from demoapi.views import *


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet, basename="users")
router.register(r'users-paged', UserPagedViewSet, basename="users-paged")
router.register(r'annotations', AnnotationViewSet, basename="annotations")
router.register(r'annotations-paged', AnnotationsPagedViewSet, basename="annotations-paged")
router.register(r'pulses', PulseSearchViewSet, basename='pulses')
router.register(r'pulses-full', PulseViewSet, basename="pulses-full")
router.register(r'experiments', ExperimentViewSet, basename="experiments")
router.register(r'experiments-paged', ExperimentPagedViewSet, basename="experiments-paged")
router.register(r'pids', PidViewSet, basename="pids")
router.register(r'pids-paged', PidPagedViewSet, basename="pids-paged")
router.register(r'accounts', LoginView, basename="accounts")

urlpatterns = [
    url('', include(router.urls))
]
