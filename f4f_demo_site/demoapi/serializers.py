from rest_framework import serializers

from demo.models import *
from django.contrib.auth.models import User


class AnnotationSerializer(serializers.ModelSerializer):
    """
    Serializer for the Annotation model. This class includes all the fields of the Annotation model
    """

    class Meta:
        model = Annotation
        fields = ("id", "user", "text", "time", "pulse")
        depth = 1


class PidSerializer(serializers.ModelSerializer):
    """
    Serializer for the PID model, including all the model's fields except the pulses
    """

    class Meta:
        model = Pid
        fields = ("id", "pid_number", "issuer", "description", "page_url")
        depth = 1


# Serializers define the API representation.
class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for the Django auth_user class. Contains the url, username, email and is_staff fields
    """

    class Meta:
        model = User
        fields = ["id", "username", "first_name", "last_name", "email", "is_active", "date_joined", "is_superuser"]
        depth = 1


class PulseSummarySerializer(serializers.ModelSerializer):
    """
    Short serializer for the Pulse model. It contains only the necessary fields for the pulses list view
    """
    annotations = AnnotationSerializer(source='get_annotations', many=True, read_only=True)

    class Meta:
        model = Pulse
        fields = ('id', 'shot', 'number', 'experiment', 'owner', 's_idsProperties_creationDate', 'annotations')
        depth = 1


class PulseSerializer(serializers.ModelSerializer):
    """
    Full pulse serializer. This class contains all the fields of the Pulse model
    """

    pids = PidSerializer(source='get_pids', many=True, read_only=True)
    annotations = AnnotationSerializer(source='get_annotations', many=True, read_only=True)

    class Meta:
        model = Pulse
        fields = ("id", "shot", "number", "experiment", "owner", "s_code_commit", "s_code_name", "s_code_repository",
                  "s_globalQuantities_b0_value", "s_globalQuantities_b0_valueErrorUpper",
                  "s_globalQuantities_b0_source", "s_heatingCurrentDrive_powerNbi_value",
                  "s_heatingCurrentDrive_powerNbi_valueErrorUpper", "s_heatingCurrentDrive_powerNbi_source",
                  "s_globalQuantities_r0_value", "s_globalQuantities_r0_valueErrorUpper",
                  "s_fusion_neutronFluxes_total_source", "s_fusion_neutronFluxes_total_value",
                  "s_globalQuantities_betapol_value", "s_globalQuantities_betaTor_value",
                  "s_globalQuantities_currentOhm_value", "s_globalQuantities_energyBFieldPol_value",
                  "s_globalQuantities_energyDiamagnetic_value", "s_globalQuantities_energyTotal_value", "s_timeWidth",
                  "s_time", "s_globalQuantities_ip_value", "s_globalQuantities_ip_valueErrorUpper",
                  "s_globalQuantities_ip_source", "s_globalQuantities_li_value", "s_globalQuantities_tauEnergy_value",
                  "s_globalQuantities_volume_value", "s_globalQuantities_vLoop_value", "s_idsProperties_creationDate",
                  "s_idsProperties_homogeneousTime", "s_lineAverage_zeff_source", "s_volumeAverage_nE_source",
                  "s_volumeAverage_nE_value", "cannots", "pids", "annotations")
        depth = 1


class ExperimentSerializer(serializers.ModelSerializer):
    """
    Serializer for the Experiment model, including all the model's fields
    """

    class Meta:
        model = Experiment
        fields = ("id", "short_name", "url")
