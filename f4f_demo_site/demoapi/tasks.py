from __future__ import absolute_import, unicode_literals
import logging

import shutil
from datetime import datetime
from os import getcwd
from os import listdir, environ
from os.path import join, exists

import pandas as pd
import pytz
from celery import shared_task

from demo.models import *

logger = logging.getLogger(__name__)


# @shared_task
def create_pulses(username):
    """
    Celery task to create pulses from CSV, NetCDF etc files

    Args
        | username (str): the name of the user who will store the data
    """
    print("Started celery task!")
    logger.debug("Started celery task!")
    data_base_path = join(getcwd(), "temp")
    machines = listdir(data_base_path)
    for machine in machines:
        if machine == "AUG":
            print("Before populating")
            populate_db_aug_data(data_base_path=data_base_path, username=username, machine=machine)
    if exists(join(getcwd(), "temp")):
        shutil.rmtree(join(getcwd(), "temp"))


def populate_db_aug_data(data_base_path, username, machine):
    """
    Function to update DB with AUG data. Checks if the machine already exists in the DB, otherwise it creates a new
    DB entry in the experiment table. The function reads the provided CSV files and stores the relevant pulses.

    Args
        | data_base_path (str): path to the temporary folder with the machine folders
        | username (str): the username of the user that performs the update
        | machine (str): the name of the machine folder(i.e. AUG)
    """
    csv_files = listdir(join(data_base_path, machine))
    summary_df = None
    summary_time_df = None
    folder_path = join(data_base_path, machine)
    if csv_files and len(csv_files) == 2:
        for csv_file in csv_files:
            if "time" in csv_file:
                summary_time_df = pd.read_csv(join(folder_path, csv_file), header=0)
                summary_time_df = summary_time_df.fillna('')
            else:
                summary_df = pd.read_csv(join(folder_path, csv_file), header=0)
                summary_df = summary_df.fillna('')
        for row_index, row in summary_df.iterrows():
            summary_time_shots = summary_time_df.loc[summary_time_df['shot'] == row["shot"]]
            number = row["number"] if "number" in summary_df.columns else 0
            pulse = Pulse()
            pulse.shot = row["shot"]
            pulse.number = number
            pulse.owner = User.objects.get(username=username)
            try:
                experiment = Experiment.objects.get(short_name=machine)
            except(BaseException, Exception) as e:
                print(e)
                host = environ["F4F_DEMO_SERVICE_HOST"] if "F4F_DEMO_SERVICE_HOST" in environ.keys() else "localhost"
                port = environ["F4F_DEMO_SERVICE_PORT"] if "F4F_DEMO_SERVICE_PORT" in environ.keys() else 8000
                url = "http://{}:{}/experiments/".format(host, port)
                experiment = Experiment(short_name=machine, url=url)
                experiment.save(username=username)
                experiment.url = url + str(experiment.id)
                experiment.save(username=username)
            pulse.experiment = experiment
            pulse.s_globalQuantities_b0_source = row["b0_source"]
            pulse.s_heatingCurrentDrive_powerNbi_source = row["power_nbi_source"]
            pulse.s_globalQuantities_ip_source = row["ip_source"]
            pulse.s_globalQuantities_r0_value = row["R0_mean"]
            pulse.s_globalQuantities_r0_valueErrorUpper = row["R0_std"]
            pulse.s_idsProperties_creationDate = pytz.utc.localize(datetime.strptime(row["date"], "%Y/%m/%d"))
            pulse.s_idsProperties_homogeneousTime = pytz.utc.localize(datetime.strptime(row["time"], "%H:%M"))
            times = list(summary_time_shots["time_max"])
            steps = list(summary_time_shots["time_width"])
            ip_values = list(summary_time_shots["ip_mean"])
            ip_upper_error = list(summary_time_shots["ip_std"])
            b0_values = list(summary_time_shots["b0_mean"])
            b0_upper_error = list(summary_time_shots["b0_std"])
            power_nbi_values = list(summary_time_shots["power_nbi_mean"])
            power_nbi_upper_error = list(summary_time_shots["power_nbi_std"])
            pulse.s_globalQuantities_ip_value = {"times": times, "values": ip_values, "steps": steps}
            pulse.s_globalQuantities_ip_valueErrorUpper = {"times": times, "values": ip_upper_error,
                                                           "steps": steps}
            pulse.s_globalQuantities_b0_value = {"times": times, "values": b0_values, "steps": steps}
            pulse.s_globalQuantities_b0_valueErrorUpper = {"times": times, "values": b0_upper_error,
                                                           "steps": steps}
            pulse.s_heatingCurrentDrive_powerNbi_value = {"times": times, "values": power_nbi_values,
                                                          "steps": steps}
            pulse.s_heatingCurrentDrive_powerNbi_valueErrorUpper = {"times": times, "steps": steps,
                                                                    "values": power_nbi_upper_error}
            pulse.save(username=username)
