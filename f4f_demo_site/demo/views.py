from django.http import HttpResponse
from django.shortcuts import redirect


def index(request):
    # return HttpResponse("Hello, world. You're at the demo index.")
    return redirect("ui/", request, permanent=True)
