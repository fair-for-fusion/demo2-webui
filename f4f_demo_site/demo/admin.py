from django.contrib import admin
from demo.models import Pulse, Annotation, Experiment

admin.site.site_header = 'FAIR4Fusion AltDemo'

admin.site.register(Annotation)
admin.site.register(Experiment)


class AnnotationInline(admin.TabularInline):
    model = Annotation


@admin.register(Pulse)
class PulseAdmin(admin.ModelAdmin):
    model = Pulse
    inlines = [
        AnnotationInline,
    ]
