from __future__ import absolute_import, unicode_literals

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app
from f4f_demo_site import settings

__all__ = ('celery_app', settings)

__version__ = "1.0.0"
