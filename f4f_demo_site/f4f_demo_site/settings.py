from keycloak_oidc.default_settings import *
import json
from os.path import join, exists, dirname, realpath, abspath
from os import getcwd, listdir, environ, pardir, mkdir
from django.conf import settings

"""
This file contains the configuration of the F4F Demonstrator. There should be available a json configuration file
for the dynamic configuration part.

The remaining configuration includes Django specific parameters, configuration of the RESTful Web Service, swagger
and logging configuration.

Apart for Django specific configuration, the json file is used in order to configure application specific parameters.
Such configuration is for example the plots section in the configuration file. The necessary configuration requires
the parameterization of the width and height of the plots to be created and the names of the fields that should be
plotted.
"""


def load_properties():
    """
    Loads a json file with settings and database configuration. In the repository, there is a file named
    examples_properties.json. You should copy the file and name it properties.json to apply your custom configuration.
    If the file properties.json is not available, the example file will be used instead.

    The configuration parameters that should be included are:

        * SECRET_KEY: the Django's secret key
        * DATABASES: the configuration for the database. Check the example_properties.json for more info
        * DEBUG: boolean parameter if the service should run in debug node
        * ALLOWED_HOSTS: allowed IPs to the service

    Returns
        | dict: a dictionary containing the content of the configuration file
    """
    dir_path = dirname(realpath(__file__))
    properties_file_path = join(dir_path, "properties.json") if exists(join(dir_path, "properties.json")) else \
        join(dir_path, "example_properties.json")
    with open(properties_file_path) as f:
        return json.loads(f.read())


def get_f4f_host_port():
    if "F4F_DEMO_SERVICE_HOST" in environ.keys():
        host = environ["F4F_DEMO_SERVICE_HOST"]
    elif "F4F_HOST" in properties.keys():
        host = properties["F4F_HOST"]
    else:
        host = "localhost"
    port = environ["F4F_DEMO_SERVICE_PORT"] if "F4F_DEMO_SERVICE_PORT" in environ.keys(
    ) else "8000"
    return host, port


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = dirname(dirname(abspath(__file__)))

all_properties = load_properties()
properties = all_properties["settings"]

# f4f base url
f4f_host, f4f_port = get_f4f_host_port()
API_BASE_URL = "http://{}:{}".format(f4f_host, f4f_port)

SECRET_KEY = properties["SECRET_KEY"] if properties[
    "SECRET_KEY"] else "8b1+3b*41xrqoc=!v4x1c=h6=2du01s^%r4x_s5*mrwcr(ci_3"

database_to_use = properties["use_database"]
database = properties["DATABASES"][database_to_use]

if database:
    try:
        database["HOST"] = environ["F4F_DEMO_DB_SERVICE_HOST"]
        database["PORT"] = environ["F4F_DEMO_DB_SERVICE_PORT"]
    except (KeyError, Exception):
        pass

DATABASES = {
    'default': {
        'ENGINE': database["ENGINE"],
        'NAME': database["NAME"],
        'USER': database["USER"],
        'PASSWORD': database["PASSWORD"],
        'HOST': database["HOST"],
        'PORT': database["PORT"],
        'OPTIONS': database["OPTIONS"],
        'TEST': database["TEST"]
    }
}

DEBUG = properties["DEBUG"]

ALLOWED_HOSTS = properties["ALLOWED_HOSTS"]

# Application definition

INSTALLED_APPS = [
    'demo.apps.DemoConfig',
    'demoapi.apps.DemoapiConfig',
    'demo_ui.apps.DemoUiConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_mysql',
    'rest_framework',
    'rest_framework_swagger',
    'keycloak_oidc',
]

KEYCLOAK_OIDC_PROFILE_MODEL = 'django_keycloak.RemoteUserOpenIdConnectProfile'
KEYCLOAK_REMOTE_USER_MODEL = 'django_keycloak.remote_user.KeycloakRemoteUser'
AUTHENTICATION_BACKENDS = (
    'keycloak_oidc.auth.OIDCAuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend',
)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'mozilla_django_oidc.middleware.SessionRefresh',
]

ROOT_URLCONF = 'f4f_demo_site.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [join(BASE_DIR, "templates")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'libraries': {  # Adding this section should work around the issue.
                'staticfiles': 'django.templatetags.static',
            },
        },
    },
]

WSGI_APPLICATION = 'f4f_demo_site.wsgi.application'

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

SESSION_SAVE_EVERY_REQUEST = True

# Internationalization
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
STATIC_ROOT = join(BASE_DIR, "static")
STATICFILES_DIRS = [join(BASE_DIR, "static_files")]

# Demonstrator version
__version__ = "1.0.0"

# Default pagination:
REST_FRAMEWORK = {
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    # 'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 20,
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'mozilla_django_oidc.contrib.drf.OIDCAuthentication',
        # 'rest_framework.authentication.SessionAuthentication',
    ],
}


if not exists(join(BASE_DIR, "logs")):
    mkdir(join(BASE_DIR, "logs"))

SWAGGER_SETTINGS = {
    "exclude_namespaces": [],  # List URL namespaces to ignore
    "api_version": '0.1',  # Specify your API's version
    "api_path": "/",  # Specify the path to your API not a root level
    "enabled_methods": [  # Specify which methods to enable in Swagger UI
        'get',
        'post',
        'put',
        'delete'
    ],
    "is_authenticated": False,  # Set to True to enforce user authentication,
    "is_superuser": False,  # Set to True to enforce admin only access
    "permission_denied_handler": None,  # If user has no permisssion, raise 403 error
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(BASE_DIR, "logs", "logfile"),
            'maxBytes': 50000,
            'backupCount': 2,
            'formatter': 'standard',
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'logfile'],
            'propagate': True,
            'level': 'WARN',
        },
        'django.db.backends': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'demo': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        },
    }
}


OIDC_OP_AUTHORIZATION_ENDPOINT = 'http://accounts.fair4fusion.iit.demokritos.gr/auth/realms/f4f/protocol/openid-connect/auth'
OIDC_OP_TOKEN_ENDPOINT = 'http://accounts.fair4fusion.iit.demokritos.gr/auth/realms/f4f/protocol/openid-connect/token'
OIDC_OP_USER_ENDPOINT = 'http://accounts.fair4fusion.iit.demokritos.gr/auth/realms/f4f/protocol/openid-connect/userinfo'
OIDC_OP_JWKS_ENDPOINT = 'http://accounts.fair4fusion.iit.demokritos.gr/auth/realms/f4f/protocol/openid-connect/certs'
OIDC_OP_LOGOUT_ENDPOINT = 'http://accounts.fair4fusion.iit.demokritos.gr/auth/realms/f4f/protocol/openid-connect/logout'

OIDC_RP_CLIENT_ID = 'f4f-ui'
OIDC_RP_CLIENT_SECRET = '79b80fd7-e40c-4b4a-836f-fd075c01e806'
OIDC_STORE_ACCESS_TOKEN = True

# URLs, HOSTs etc
# LOGIN_URL = 'keycloak_login'
# DARE login
# LOGIN_HOST = environ["DARE_LOGIN_PUBLIC_SERVICE_HOST"] if "DARE_LOGIN_PUBLIC_SERVICE_HOST" in environ.keys() else \
#     "localhost"
# LOGIN_PORT = environ["DARE_LOGIN_PUBLIC_SERVICE_PORT"] if "DARE_LOGIN_PUBLIC_SERVICE_PORT" in environ.keys() else 5001

# LOGIN_URL = "http://accounts.fair4fusion.iit.demokritos.gr/auth/"

# DARE Exec API
EXEC_API_HOST = environ["EXEC_API_PUBLIC_SERVICE_HOST"] if "EXEC_API_PUBLIC_SERVICE_HOST" in environ.keys() else \
    "localhost"
EXEC_API_PORT = environ["EXEC_API_PUBLIC_SERVICE_PORT"] if "EXEC_API_PUBLIC_SERVICE_PORT" in environ.keys(
) else 80

EXEC_API_URL = "https://platform.dare.scai.fraunhofer.de/exec-api" if EXEC_API_HOST == "localhost" else \
    "http://{}:{}".format(EXEC_API_HOST, EXEC_API_PORT)

# rabbitmq
rabbitmq_host = environ["RABBITMQ_SERVICE_HOST"] if "RABBITMQ_SERVICE_HOST" in environ.keys(
) else "localhost"
rabbitmq_port = environ["RABBITMQ_SERVICE_PORT"] if "RABBITMQ_SERVICE_PORT" in environ.keys(
) else "5672"

BROKER_URL = "amqp://f4f:f4f@{}:{}//".format(rabbitmq_host, rabbitmq_port)

CELERY_APP = "f4f_demo_site"
CELERY_BIN = "/usr/bin/celery"
CELERYD_LOG_LEVEL = "DEBUG"
CELERY_TIMEZONE = TIME_ZONE
CELERY_RESULT_BACKEND = 'amqp'
CELERY_ALWAYS_EAGER = False

# auth_uri = "http://accounts.fair4fusion.iit.demokritos.gr/auth/realms/f4f"
# client_id = "f4f-ui"
# public_uri = "http://accounts.fair4fusion.iit.demokritos.gr"

# OIDC_OP_AUTHORIZATION_ENDPOINT = auth_uri + '/protocol/openid-connect/auth'
# OIDC_OP_TOKEN_ENDPOINT = auth_uri + '/protocol/openid-connect/token'
# OIDC_OP_USER_ENDPOINT = auth_uri + '/protocol/openid-connect/userinfo'
# LOGIN_REDIRECT_URL = public_uri + 'v1/mgmt'
# LOGOUT_REDIRECT_URL = auth_uri + \
#     '/protocol/openid-connect/logout?redirect_uri=' + public_uri
# OIDC_RP_CLIENT_ID = client_id
# OIDC_RP_CLIENT_SECRET = ''
# OIDC_RP_SCOPES = 'email openid profile'
# OIDC_RP_SIGN_ALGO = 'RS256'
# OIDC_OP_JWKS_ENDPOINT = auth_uri + '/protocol/openid-connect/certs'

# # Fields to look for in the userinfo returned from Keycloak
# OIDC_CLAIMS_VERIFICATION = 'preferred_username sub'

# # Allow this user to not have an email address during OIDC claims verification.
# KEYCLOAK_ADMIN_USER = 'admin'

# # NOTE: scope is optional and can be left out
# configure_oidc(auth_uri, client_id, public_uri)
