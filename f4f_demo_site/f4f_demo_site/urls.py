"""f4f_demo_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from rest_framework_swagger.views import get_swagger_view

from demo_ui.views import *
from demoapi.views import *
from f4f_demo_site.settings import *

schema_view = get_swagger_view(title="Demo Dashboard")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('demo.urls')),
    path("api/", include("demoapi.urls")),
    path("ui/", include("demo_ui.urls")),
    url(r'^docs/$', schema_view),
    url(r'^keycloak/', include('keycloak_oidc.urls')),
] + static(STATIC_URL, document_root=STATIC_ROOT)
